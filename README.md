# Junction
Repository of team **Junction** for the [**Road Segmentation** project][8] (part
of the [Computational Intelligence Lab][9] at ETH Zürich).

![ensemble predictions](assets/img/predictions.png)

## Overview
We consider various UNet models obtained by plugging the convolutional layers
of well-known CNNs as their encoder part. For brevity, and because we have not
encountered any ambiguity so far, we dub them UNetXY where the suffix is the
depth (or identifier) of the corresponding CNN. For instance, UNet11 and UNet34
borrow the contracting paths from VGG11 and ResNet34, respectively. From an
implementation point of view, we draw most of our inspiration from Iglovikov et
al. Using established architectures comes with the advantage of having
pre-trained versions of them shipped with popular software frameworks.

Our best model is UNet38 which uses the convolutional layers of the
WideResNet38 network. The WideResNet architecture is characterized by fewer
layers and a higher number of channels. It has been proposed as an alternative
family of residual networks that do not suffer from the *diminishing
feature reuse* problem. In UNet38, we use the first five convolutional blocks
on the contracting path and adapt the expanding path accordingly. The
architecture is sketched below.
![ensemble predictions](assets/img/unet.png)

All of our models are trained using the Adam optimizer with a(n) (initial)
learning rate of 1e-4. In general, we obtain the best results when running for
100 to 200 epochs. We use 4 GTX 1080Ti video cards which accommodate a batch
size of 32 images. This is also made possible by a [memory-efficient
implementation proposed recently for in-place activated batch
normalization][1]. We actually use the code accompanying the cited paper
directly, by packaging it in a standalone library (see [our fork][2]).
Additionally, the encoder part of our UNet38 model briefly described above is
initialized with the weights corresponding to the ImageNet pre-trained
WiderResNet38 that the same authors provide as part of [their repository][10].

### Framework
We designed a flexible framework for pre-processing the training data,
training, and performing inference on the test data. We use a YAML config file
as the only input. We include an example config in the root of this repository
which contains most of the settings that one can use. Additionally, we include
under the directory `config_templates/` multiple configs which are clearer and
focused on a single task.

For this config-based interface, the best documentation is the example config
`example_config.yaml`.


## Installation
The preferred way of setting up the environment for using the framework is by
creating a new `conda` environment:

```console
$ conda create -n junction
(junction) $ source activate junction
(junction) $ conda config --append channels pytorch
(junction) $ conda config --append channels conda-forge
(junction) $ conda env update —-file .environment
```

Note that we use PyTorch in our implementation, so the corresponding conda
channels have to be added. This should set up all dependencies. We also provide
a `setuptools`-based script, `setup.py`. Packaging the whole framework as a
library can be achieved by issuing `python setup.py install`. However, we note
that one of our dependencies comes from [a GitHub repository][2]
(which is our library-packaged version of the repo with the same name from
[mapillary][5]), and we did not find a way to integrate this build framework with
external repositories. That is why we prefer conda. That being said, to install
it manually, simply do

```console
(junction) $ pip3 install git+https://github.com/calincru/inplace_abn
```


## Running
In this part we will describe step-by-step how our framework can be used to
pre-process, train, and, finally, run inference on the test images. While doing
so, we will focus on the settings that led to our best results, so that the
results can be reproduced at any time.

First of all, as we have mentioned before, we use the [inplace_abn][2] package
that accompanies the paper which describes how the training can be made more
memory-efficient by running the batch-normalisation (common in many
architectures, including ours) and the activation in-place. From the same
package, we re-use the _WiderResNet38_ architecture, which **only runs on
CUDA-enabled GPUs**.  Also, we always use the ImageNet-pretrained version that
the same authors provide. Thus, the first step is to download the saved model
from [their GDrive][3] and place it under
`data/models/wide_resnet38_ipabn_lr_256.pth.tar`. Also, we assume that the
training and test data form a tree hierarchy as the following one:

```console
(junction) $ ls data/
data/raw/
├── testing
│   ├── 105.png
│   ├── 106.png
└── training
    ├── groundtruth
    │   ├── 001.png
    │   ├── 002.png
    └── images
        ├── 001.png
        ├── 002.png
```

Notice that the names are slightly changed from those provided [on Kaggle][4]
in that we have removed the `satImage_` part from the names and renamed the
directories as `training/` and `testing/`. To do this, you can use a script
provided under `bin/`, as follows

```console
(junction) $ python bin/rename_images.py --path data/raw/testing
(junction) $ python bin/rename_images.py --path data/raw/training/images
(junction) $ python bin/rename_images.py --path data/raw/training/groundtruth
```

Now we are technically ready to load and run our UNet architecture based on the
`WideResNet38` model. However, first let us show how pre-processing can be run.

### Pre-processing
We describe the data pre-processing (by which we actually mean *augmentation*)
as a composition of functions that receive the training data as input and
return a (possibly larger) set of pairs `(images, masks)` as output. This is
described in the YAML config file, when the `action` is set to `preprocessing`,
by filling the field `preprocessing.transforms`. The one we have used to
generate the data fed to each one of the model from our ensemble, as described
at the beginning of this document, is exactly
`config_templates/preprocess.yaml`. It consists of a series of flips with
respect to the horizontal axis, vertical axis, and the center, several
rotations (5 random ones per input image) and zoom-ins (3 random ones per input
image).

To get five different data sets, one for each ensemble member, we have
basically invoked five times our framework using this config and saved the
resulting data.  More precisely, if we want to achieve the following tree
structure:

```console
(junction) $ ls data/
data/
├── proc_9600a/
│   ├── testing/
│   └── training/
│       ├── X.npy
│       └── y.npy
├── proc_9600b/
│   ├── testing/
│   └── training/
│       ├── X.npy
│       └── y.npy
```

and so on, until `proc_9600e`, we would just modify the preprocessing config
file to output the results in the desired directory, for instance,

```yaml
save_dir_root: './data/proc_9600d'
```

for the 4th one, and all the other settings remain unchanged. However, we also
want to make sure that they are generated with different random seeds. Our
framework by default sets a fixed seed, so all the results are technically
100% reproducible.  Thus, to force it to use a system-default seed, which
should be different from run to run, we added a CLI flag, so each one of the
five preprocessing runs should be invoked as follows:

```console
(junction) $ python run.py --config config_templates/preprocess.yaml --no_seed  # <-- notice additional flag
```

This will take some time (about 20 minutes per run) and each one of the five
derived datasets will contain 9600 samples, taking roughly 6GB of disk space.

### Training
As expected, the training of our UNet models is very expensive. When running
directly on raw data (which also yields decent results, as we note in the
report) it is quite fast, one epoch taking about 10 seconds. However, when
training on the data we have generated in the previous step, one pass through
it takes about 12 minutes when using 2 GTX 1080Ti video cards and a batch size
of 16.

The training runs for 100 epochs. It saves snapshots of the network every 25
epochs, so if the loss seems to plateau significantly, it can also be
interrupted earlier. We did not do so because we have noticed that even if it
plateaus, we can still sometimes see improvements in the predictions if we let
it train for longer.

With that in mind, launching the training is as easy as doing

```console
(junction) $ python run.py --config config_templates/train.yaml --no_seed
```

after appropriately setting the source data path in the config (as well as the
save directory, to avoid manually moving it afterwards)

```yaml
save_dir_root: 'data/models/unet38-9600d/'

data:
  type: preprocessed
  path: 'data/proc-9600d'
```

You will also notice a significant time spent just by loading the data in
memory and then copying it go GPUs (2 minutes when using 20 active threads).
Besides the huge GPU load, the training takes about 80GB of RAM too. A common
output pattern looks as follows:

```
[2018-06-26 23:35:35,217][run.py:67]: Source data shape: ([9600, 400, 400, 3], [9600, 400, 400])
[2018-06-26 23:40:23,873][run.py:97]: Post-processing data shape: ([9600, 3, 416, 416], [9600, 1, 416, 416])
[2018-06-26 23:40:32,046][train.py:173]: Using CUDA for training
[2018-06-26 23:53:55,367][train.py:211]: epoch 1, train loss 0.10462
[2018-06-27 00:07:17,810][train.py:211]: epoch 2, train loss 0.13410
```

Lastly, to monitor the loss, we have integrated our PyTorch-based
implementation with TensorFlow's [TensorBoard][6]. In the above example, it can
be run as follows:

```console
(junction) $ tensorboard --logdir ./data/models/unet38-9600d/ --port 9966
```

and then navigate to http://localhost:9966. Remember that if a new
tab/tmux-split is open, then `source activate junction` has to be run again. If
using the exact training config we are providing, then the only thing that is
monitored is the training loss.  However, if some data is kept aside for
validation (see `example_config.yaml` for which fields need to be modified to
aciheve this), multiple metrics are tracked, such as the precision, recall, f1
score, accuracy, train loss, validation loss.

#### Training Output
Before showing how test predictions are obtained, let us first briefly discuss
the contents of the save directory after a training session is run.

```console
(junction) $ tree data/models/unet38-9600d
data/models/unet38-9600d
├── config.yaml
├── events.out.tfevents.1530009315.matroid1
├── net_25.pth
├── net_50.pth
├── net_75.pth
├── net_100.pth
├── X_test_ids.txt
└── X_val_ids.txt
```

The config file is just the config used in this run of the framework. It is
useful when certain aspects of it are forgotten. The events file is used by
TensorBoard. The 4 following files are snapshots of the model (only its state
dict, to make it smaller) after different number of epochs have been run. The
last two files contain the identifiers of the images that are part of the test
set and validation set, respectively. This was useful for inspecting the
predictions on the validation set using our scripts from `bin/`, such as
`bin/plot_prediction_histogram.py`.

#### Trained models
Because training a single model can be very expensive (approximately one day
for 100 epochs, when running on the 9600-sized pre-processed data set and using
2 GPUs), let alone doing it for 5 times, we include the saved models [here][7].
They can be retrieved as follows

```console
(junction) $ mkdir -p ./data/models
(junction) $ wget -r -np -nH --cut-dirs=3 -R "index.html*" -e robots=off http://swarm.cs.pub.ro/~ccruceru/cil/models/ensemble -P ./data/models/
(junction) $ find ./data/models/ensemble -type f -exec md5sum {} \; | sort -k 2 | md5sum -c ./data/models/ensemble.md5
```

Note that the naming is a little bit different than we have used so far across
this README. We will stick to this one from now on. We next show how they can
be used for inference.


### Inference
For inference, we provide two main _modes_ which are determined by the output
type. The former, which we used most often either because we wanted to inspect
the predictions or for ensembling purposes (see next section), outputs the
predictions as numpy arrays. We call it the _numpy_ format. The corresponding
config file is `config_templates/inference_to_labels.yaml`.

The testing data is always input in raw format and we do not run any
transformation on it. The only pre-inference hooks are just meant to package it
in the expected format, i.e., as PyTorch tensors normalized as expected by the
model. Thus, the only field which needs to be changed in the config file is the
snapshot path:

```yaml
save_dir_directory: 'data/predictions/ensemble/d/100/'
inference:
    snapshot_path: 'data/models/ensemble/d/net_100.pth'
```

Depending on the GPU memory, the batch size might also need to be adjusted.

The second mode outputs directly the CSV submission format. We call it the
_csv_ format. This is done just by changing a post-inference hook. We added it
to our config templates too to avoid repeatedly going back and forth between
the two (`config_templates/inference_to_submission.yaml`).

#### Inference Output
As for training, we also discuss the contents of the output directory for an
inference session. We show below the output for the case when _numpy_
predictions are output. The one which directly outputs the submission format
contains a `submission.csv` file instead of the `*.npy` files that follow.

```console
(junction) $ tree data/predictions/ensemble/d/100/
data/predictions/ensemble/d/100/
├── config.yaml
├── X_test_ids.txt
├── X_train_ids.txt
├── y_pred.npy
├── y_pred_test_raw.npy
└── y_pred_train_raw.npy
```

As always, the config file is the one used to set up the framework for that
run. The next two files contain the identifiers of the train and test images.
The ones for test are needed when submitting to Kaggle. The ones for the train
set are added just for consistency, they have not really been used. The last
three files are the most important ones. Their name are quite self-explanatory.
The first one, `y_pred.npy`, contains the _binary_ predictions. The second one
contains the _raw_ predictions (i.e. real numbers between 0 and 1) on the test
set, which we have often together with `bin/sample_plot.py` to inspect the
areas of uncertainty. They are also the ones used when averaging the ensemble
(see next section). Finally, the last one contains the predictions on the train
points (by train here we mean the entire train dataset, which includes the
validation set too). This allows us to see how well the network fits the
training set.


### Ensemble
As mentioned before, our best results were obtained by averaging the
predictions of five UNet38 models trained on five different data sets, all of
which obtained using the same transformations but with different seeds.
Moreover, we have noticed that after several training epochs the loss does not
decrease anymore and the network oscillates between several _good_ states.
Probably because of that, we also noticed that averaging over the predictions
given by the network snapshots obtained after 50, 75, and 100 epochs yields
more robust overall predictions. Therefore, our final predictions are obtained
by averaging a total of 15 raw predictions.

To do this, we have created a script ([bin/ensemble.py](./bin/ensemble.py))
that takes a list of inference output directories (which have to contain the
files describe din the previous subsection; in particular, the ids file and the
raw test prediction file are important), a class-label threshold used to map
real numbers between 0 and 1 to binary class labels, and an output format which
has the same meaning as described in the Inference.  To average the 15 raw
predictions we call it as follows:

```console
(junction) $ python bin/ensemble.py --threshold 0.3 --output_format csv --paths \\
  ./data/predictions/ensemble/a/50/  \\
  ./data/predictions/ensemble/a/75/  \\
  ./data/predictions/ensemble/a/100/ \\
  ./data/predictions/ensemble/b/50/  \\
  ./data/predictions/ensemble/b/75/  \\
  ./data/predictions/ensemble/b/100/ \\
  ./data/predictions/ensemble/c/50/  \\
  ./data/predictions/ensemble/c/75/  \\
  ./data/predictions/ensemble/c/100/ \\
  ./data/predictions/ensemble/d/50/  \\
  ./data/predictions/ensemble/d/75/  \\
  ./data/predictions/ensemble/d/100/ \\
  ./data/predictions/ensemble/e/50/  \\
  ./data/predictions/ensemble/e/75/  \\
  ./data/predictions/ensemble/e/100/
```

This will output a `submission.csv` file which can be uploaded directly to
_kaggle_. One thing worth noting here is that we have found the overall
F1-score that our predictions achieve to be quite insensitive to the threshold
value, in the sense that the network is in general very confident if a pixel
belongs to a road or not, so there are very few _uncertain_ areas.  This
explains why we haven't spent too much time looking for _an optimal one_.

Also, if instead we passed `--output_format npy`, the script would output a
file named `y_pred_ensemble.npy` which can be used to plot or inspect them in
some other ways. For instance, to obtain the predictions plot included at the
beginning of this document, we used:

```console
(junction) $ python bin/sample_plot.py --predictions_file y_pred_ensemble.npy
```

[1]: https://arxiv.org/abs/1712.02616
[2]: https://github.com/calincru/inplace_abn
[3]: https://drive.google.com/file/d/1T1kGNg-W1gxvUTjSVRUiFI3CNwxe4Lb5/view
[4]: https://www.kaggle.com/c/cil-road-segmentation-2018/data
[5]: https://github.com/mapillary
[6]: https://www.tensorflow.org/programmers_guide/summaries_and_tensorboard
[7]: http://swarm.cs.pub.ro/~ccruceru/cil/
[8]: https://www.kaggle.com/c/cil-road-segmentation-2018
[9]: http://da.inf.ethz.ch/teaching/2018/CIL/
[10]: https://github.com/mapillary/inplace_abn
