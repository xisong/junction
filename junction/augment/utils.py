import os

import numpy as np
from PIL import Image

from ..utils import check_mkdir


def show_image(z):
    r"""Show image z
    Be careful when calling this function for every image!
    """
    if len(z.shape) > 2 and z.shape[0] == 1 and np.max(z) == 1:
        z = np.squeeze(z, axis=0)
        z = 255 * z
        z = z.astype(np.uint8)

    if z.shape == 3:
        img = Image.fromarray(z, 'RGB')
    else:
        img = Image.fromarray(z)
    img.show()


def save_image(z, location, filename, image_type, which):
    r"""Save image z in path.
    The path will be 'location/which/filename.type'.
    """
    # create transformation folder if it does not exist
    check_mkdir(os.path.join(location, which), increment=False)
    path = os.path.join(location, which, '{}.{}'.format(filename, image_type))

    if len(z.shape) > 2 and z.shape[0] == 1 and np.max(z) == 1:
        z = np.squeeze(z, axis=0)
        z = 255 * z
    z = z.astype(np.uint8)

    if z.shape == 3:
        img = Image.fromarray(z, 'RGB')
    else:
        img = Image.fromarray(z)
    img.save(path)


def is_float(element):
    r"""Checks if a value can be cast to float."""
    try:
        float(element)
        return True
    except TypeError:
        return False
