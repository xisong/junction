r"""This module defines various closures used mainly for preprocessing by
transforming all entries of our dataset. (nd to generate new training images)
"""
# They are in order but pylint does not recognize cv2!
# pylint: disable=wrong-import-order
from abc import abstractmethod, ABC

from absl import flags, logging
import cv2
import numpy as np
import scipy.misc
import scipy.ndimage
import skimage.transform
from sklearn.utils import check_random_state
import torch
from torchvision.transforms import Compose as TorchvisionCompose

from .utils import is_float, save_image
from ..utils import shape

FLAGS = flags.FLAGS

# flags defined for various classes in this module
flags.DEFINE_integer('debug_save_samples', 10,
                     'Number of processed samples to save. One by default.')

###############################################################################
## BASE/COMPOSITE/META CLASSES
###############################################################################


class Transform(ABC):
    r"""Base class for project-specific data transforms."""

    def __init__(self):
        self.id = '{}_{}'.format(type(self).__name__, id(self))

    @abstractmethod
    def __call__(self, data):
        r"""This method has to be overridden by all implementing classes."""
        pass


class DebugEnable(Transform):
    r"""A meta-class which wraps another transform with a debug hook."""

    class _DebugHook(Transform):
        r"""The debug hook which can be configured to do various things after a
        transformer is run.
        """

        def __init__(self, transform, suffix=None):
            r"""Initializes this debug hook, given a preceeding transform."""
            super().__init__()
            self.t = transform
            self.suffix = '' if suffix is None else suffix
            self.rand = check_random_state(FLAGS.debug_random_state)

        def __call__(self, data):
            # ignore inputs which are not tuples consisting of two numpy arrays
            if not (isinstance(data, tuple) and len(data) == 2) or \
                    not (isinstance(data[0], np.ndarray) or \
                         isinstance(data[1], np.ndarray)):
                logging.warning(
                        'Do not know how to handle input of type %s. '
                        'Doing nothing...', type(data))
                return data

            # unpack it
            X, y = data
            assert shape(X)[0] == shape(y)[0]
            n = shape(X)[0]

            # save a few of them, as specified
            indices = self.rand.choice(n, FLAGS.debug_save_samples)
            for idx in indices:
                if X is not None:
                    save_image(X[idx], FLAGS.debug_save_dir, 'x_{}_{}'.format(
                            idx, self.suffix), 'png', self.t.id)
                if y is not None:
                    save_image(y[idx], FLAGS.debug_save_dir, 'y_{}_{}'.format(
                            idx, self.suffix), 'png', self.t.id)

            # simply forward it
            return data

    def __init__(self, t):
        super().__init__()
        self.t = TorchvisionCompose([
                DebugEnable._DebugHook(t, suffix='before'), t,
                DebugEnable._DebugHook(t, suffix='after')
        ])

    def __call__(self, data):
        return self.t(data)


class Compose(Transform):
    r"""A composer which can insert a debug hooks after each individual
    transform closure if debugging is enabled.
    """

    def __init__(self, transforms):
        super().__init__()
        self.t = TorchvisionCompose(
                [DebugEnable(t) if FLAGS.debugging else t for t in transforms])

    def __call__(self, data):
        return self.t(data)


class Concat(Transform):
    r"""Concatenate a sequence of transforms.

    NOTE: Every transform closure must output the same size.
    """

    def __init__(self, transforms):
        super().__init__()
        self.ts = [DebugEnable(t) if FLAGS.debugging else t for t in transforms]

    def __call__(self, data):

        def concat(data1, data2):  # pylint: disable=missing-docstring
            # if one of them is None, return the other
            if data1 is None:
                return data2
            if data2 is None:
                return data1

            X1, y1 = data1
            X2, y2 = data2

            return np.append(X1, X2, axis=0), np.append(y1, y2, axis=0)

        data_concat = None
        for t in self.ts:
            data_from_transform = t(data)
            data_concat = concat(data_from_transform, data_concat)

        return data_concat


class Identity(Transform):
    r"""Returns anything it receives as input."""

    def __call__(self, data):
        return data


class ComposeTorchvision(Transform):
    r"""Wraps a sequence of Torchvision which act only on the data and are
    agnostic of the masks (i.e. class labels).
    """

    def __init__(self, data_transforms=None, masks_transforms=None):
        super().__init__()
        self.data_transform = None if data_transforms is None \
                else TorchvisionCompose(data_transforms)
        self.masks_transform = None if masks_transforms is None \
                else TorchvisionCompose(masks_transforms)

    def __call__(self, data):

        def transform(Z, t):  # pylint: disable=missing-docstring
            r"""This function applies the transform `t` on each row in Z."""
            Z_new = []
            for z in Z:
                Z_new.append(t(z))

            # this is where it is assumed that one of the transforms converts
            # them to torch tensors.
            # FIXME(ccruceru): If this becomes cumbersome, refactor this part.
            return torch.stack(Z_new)

        # unpack data
        X, Y = data
        X_new, Y_new = X, Y

        # transform the image data
        if X is None:
            if self.data_transform:
                logging.warning('The list of data transforms is not empty, but '
                                'data was not provided!')
            X_new = None
        elif self.data_transform:
            X_new = transform(X, self.data_transform)

        # transform the ground truth masks
        if Y is None:
            if self.masks_transform:
                logging.warning('The list of ground truth masks transforms is '
                                'not empty, but no mask was provided!')
            Y_new = None
        elif self.masks_transform:
            Y_new = transform(Y, self.masks_transform)

        return X_new, Y_new


###############################################################################
## GENERAL TRANSFORM CLOSURES
###############################################################################


class Rotate(Transform):
    r"""Rotates the images by a given angle (in degrees, counterclockwise).

    For each image it will generate another $num_per_image images with a
    random angle from $min_angle to $max_angle

    The seed is used to make it reproducible (the seed is set in run.py)

    The size of the rotated image is the same as the input image
    """

    def __init__(self, min_angle, max_angle, num_per_image):
        super().__init__()
        self.min_angle = min_angle  # degrees
        self.max_angle = max_angle
        self.num_per_image = num_per_image

    def __call__(self, data):
        # unpack data
        X, Y = data

        def generate_angles(count):  # pylint: disable=missing-docstring
            angles_list = np.random.uniform(
                    low=self.min_angle, high=self.max_angle, size=(count, ))

            return angles_list

        def transform(Z, angles_list=None):  # pylint: disable=missing-docstring
            Z_new = []

            # We want to have the same angle for X and y images.
            # For X, angles_list is None, for y it is already computed.
            if angles_list is None:
                angles_list = generate_angles(Z.shape[0] * self.num_per_image)

            for i, z in enumerate(Z):
                input_height = z.shape[0]
                input_width = z.shape[1]

                for k in range(self.num_per_image):
                    # take the angle
                    angle = angles_list[i + k]

                    # rotate
                    z_new = scipy.ndimage.interpolation.rotate(z, angle)

                    # crop to the original size
                    height_diff = (z_new.shape[0] - input_height) / 2.0
                    height_diff_left = int(np.floor(height_diff))
                    height_diff_right = int(np.ceil(height_diff))

                    width_diff = (z_new.shape[1] - input_width) / 2.0
                    width_diff_left = int(np.floor(width_diff))
                    width_diff_right = int(np.ceil(width_diff))

                    if z_new.shape == 3:
                        z_new = z_new[height_diff_left:z_new.shape[0] -
                                      height_diff_right, width_diff_left:
                                      z_new.shape[1] - width_diff_right, :]
                    else:
                        z_new = z_new[height_diff_left:z_new.shape[0] -
                                      height_diff_right, width_diff_left:
                                      z_new.shape[1] - width_diff_right]

                    Z_new.append(z_new)

            return np.array(Z_new), angles_list

        # transform the image data
        X_new, angles = transform(X)

        # transform the ground truth masks
        Y_new, angles = transform(Y, angles_list=angles)

        return X_new, Y_new


class Resize(Transform):
    r"""Resize the images to a given size
        height and width define the size of the output image.
    """

    def __init__(self, new_height, new_width):
        super().__init__()
        # here new_size is str (from config). Convert it to tuple:
        self.new_height = new_height
        self.new_width = new_width

    def __call__(self, data):
        # unpack data
        X, Y = data

        def transform(Z):  # pylint: disable=missing-docstring
            Z_new = []

            for z in Z:
                z_new = skimage.transform.rescale(
                        z, (self.new_height / z.shape[0],
                            self.new_width / z.shape[1]),
                        mode='reflect')
                # Rescale because now the values are from 1 to 0
                z_new = 255 * z_new
                # Convert to integer data type pixels.
                z_new = z_new.astype(np.uint8)

                Z_new.append(z_new)

            return np.array(Z_new)

        # transform the image data
        X_new = transform(X)

        # transform the ground truth masks
        Y_new = transform(Y)

        return X_new, Y_new


class Crop(Transform):
    r"""Crop the images by a given size.
        new_height and new_width define the size of the
        the remaining (centered!) image (and they should be
        less than tha current size of the image, otherwise
        the image remains unchanged)

        E.g. if the initial size of the image is 400x400 and
        new_height is 200 and new_width is 200, then
        the resulted image has the size 200x200 (centered rectangle
        from the initial image)
    """

    def __init__(self, new_height, new_width):
        super().__init__()
        # here new_size is str (from config). Convert it to tuple:
        self.new_height = new_height
        self.new_width = new_width

    def __call__(self, data):
        # unpack data
        X, Y = data
        X_new, Y_new = X, Y

        def transform(Z):  # pylint: disable=missing-docstring
            Z_new = []

            for z in Z:
                height = z.shape[0]
                width = z.shape[1]

                height_diff = int((height - self.new_height) / 2)
                height_diff_left = int(np.floor(height_diff))
                height_diff_right = int(np.ceil(height_diff))

                width_diff = int((width - self.new_width) / 2)
                width_diff_left = int(np.floor(width_diff))
                width_diff_right = int(np.ceil(width_diff))

                if z.shape == 3:
                    z_new = z[height_diff_left:height - height_diff_right,
                              width_diff_left:width - width_diff_right, :]
                else:
                    z_new = z[height_diff_left:height - height_diff_right,
                              width_diff_left:width - width_diff_right]

                Z_new.append(z_new)

            return np.array(Z_new)

        # transform the image data
        X_new = transform(X)

        # transform the ground truth masks
        if Y is not None:
            Y_new = transform(Y)

        return X_new, Y_new


class Zoom(Transform):
    r"""This transformation acts as a magnifier
    First scales and then crop the image
    Scale factor should be higher than 1.0 (we want to zoom in)

    E.g. of centers (used for crop)
    if center_height=0.5 and center_width=0.5 -> rectangle centered
    if center_height=0.0 and center_width=0.0 -> rectangle top-left
    if center_height=1.0 and center_width=1.0 -> rectangle bottom-right

    If scale_factor, center_height, center_width are intervals, then
    the transform will generate $num_per_image images with random params

    TODO if scale_factor, center_height, center_width are floats, then
    the parameter num_per_image is ignored (it will generate one image
    deterministically)
    """

    def __init__(self, output_height, output_width, scale_factor, center_height,
                 center_width, num_per_image):
        super().__init__()
        self.output_height = output_height
        self.output_width = output_width
        self.scale_factor = scale_factor
        self.center_height = center_height
        self.center_width = center_width
        self.num_per_image = num_per_image

    def __call__(self, data):
        # unpack data
        X, Y = data

        def generate_params(count):  # pylint: disable=missing-docstring
            scale_factors = np.random.uniform(
                    low=self.scale_factor[0],
                    high=self.scale_factor[1],
                    size=(count, ))
            centers_height = np.random.uniform(
                    low=self.center_height[0],
                    high=self.center_height[1],
                    size=(count, ))
            centers_width = np.random.uniform(
                    low=self.center_width[0],
                    high=self.center_width[1],
                    size=(count, ))
            centers = centers_height, centers_width

            return scale_factors, centers

        def transform(Z, scale_factors=None, centers=None):  # pylint: disable=missing-docstring
            Z_new = []

            # We want to have the same angle for X and y images.
            # For X, angles_list is None, for y it is already computed.
            if scale_factors is None or centers is None:
                scale_factors, centers = \
                    generate_params(Z.shape[0]*self.num_per_image)

            for i, z in enumerate(Z):
                for k in range(self.num_per_image):
                    # take the scale factor
                    scale_factor = scale_factors[i + k]
                    center_height = centers[0][i + k]
                    center_width = centers[1][i + k]

                    # scale
                    z_new = skimage.transform.rescale(
                            z, (scale_factor, scale_factor))
                    # Rescale because now the values are from 1 to 0
                    z_new = 255 * z_new
                    # Convert to integer data type pixels.
                    z_new = z_new.astype(np.uint8)

                    # crop to the desired size
                    height_diff = z_new.shape[0] - self.output_height
                    height_diff_left = int(height_diff * center_height)
                    height_diff_right = height_diff - height_diff_left

                    width_diff = z_new.shape[1] - self.output_width
                    width_diff_left = int(width_diff * center_width)
                    width_diff_right = width_diff - width_diff_left

                    if z_new.shape == 3:
                        z_new = z_new[height_diff_left:z_new.shape[0] -
                                      height_diff_right, width_diff_left:
                                      z_new.shape[1] - width_diff_right, :]
                    else:
                        z_new = z_new[height_diff_left:z_new.shape[0] -
                                      height_diff_right, width_diff_left:
                                      z_new.shape[1] - width_diff_right]

                    Z_new.append(z_new)

            return np.array(Z_new), scale_factors, centers

        # transform the image data
        X_new, scale_factors_list, centers_list = transform(X)

        # transform the ground truth masks
        Y_new, scale_factors_list, centers_list = \
            transform(Y, scale_factors=scale_factors_list,
                      centers=centers_list)

        return X_new, Y_new


class MirrorPadDivisible(Transform):
    r"""This transform makes sure that the input images have the proper size,
    as expected by the UNet architectures. More precisely, most of them assume
    that their size is a multiple of 32.
    """

    # NOTE: We store the padding offsets to be accessed after this closure is
    # run.
    last_paddings = None

    def __init__(self, div, pad_y=True):
        super().__init__()
        self.div = div
        self.pad_y = pad_y

    def __call__(self, data):
        X, y = data
        height, width, _ = X[0].shape
        paddings = self._get_paddings(height, width)

        # save the offsets if padding y is not requested
        if not self.pad_y:
            MirrorPadDivisible.last_paddings = paddings

        def transform(Z):  # pylint: disable=missing-docstring
            Z_new = []
            for z in Z:
                z_new = cv2.copyMakeBorder(z, *paddings, cv2.BORDER_REFLECT_101)
                Z_new.append(z_new)

            return np.array(Z_new)

        X_new = transform(X)
        if y is None or not self.pad_y:
            return X_new, y
        # NOTE: If provided, the masks must have the same shape as the original
        # images.

        return X_new, transform(y)

    def _get_paddings(self, height, width):
        if height % self.div == 0:
            y_min_pad = 0
            y_max_pad = 0
        else:
            y_pad = self.div - height % self.div
            y_min_pad = int(y_pad / 2)
            y_max_pad = y_pad - y_min_pad

        if width % self.div == 0:
            x_min_pad = 0
            x_max_pad = 0
        else:
            x_pad = self.div - width % self.div
            x_min_pad = int(x_pad / 2)
            x_max_pad = x_pad - x_min_pad

        return y_min_pad, y_max_pad, x_min_pad, x_max_pad


class SquashMasks(Transform):
    r"""This is used to transform the ground truth images. By default, even
    though we only have two classes, they are not binary.
    """

    def __init__(self, method='threshold', **kwargs):
        super().__init__()
        self.method = method

        # knowns methods
        if method not in ['threshold', 'max_norm']:
            raise ValueError(
                    'The only supported mask squashing methods are: '
                    'threshold', 'max_norm')

        # sanity checks for the method required params
        if method == 'threshold':
            if 'threshold' not in kwargs:
                raise ValueError(
                        'When using the "threshold" method in `SquashMasks`, '
                        'an additional "threshold" argument has to be passed!')
            else:
                threshold = kwargs['threshold']
                if threshold < 0.0 or threshold > 1.0:
                    raise ValueError('The threshold must be between 0 and 1.')
                self.threshold = threshold

    def __call__(self, data):
        X, Y = data
        if Y is None:
            logging.warning('Masks missing in the `SquashMasks` transform. '
                            'Nothing to do...')
            return data

        # max value in Y
        Y_max = np.max(Y)

        def transform(y):  # pylint: disable=missing-docstring
            # we don't have any other method for now
            if self.method == 'threshold':
                y_new = np.copy(y)
                y_new[y_new > self.threshold] = 1.0
            else:
                assert self.method == 'max_norm'
                y_new = y / Y_max

            return y_new

        # run the transformation according to the configured method
        Y_new = []
        for y in Y:
            y_new = transform(y)
            Y_new.append(y_new)
        Y_new = np.array(Y_new)

        return X, Y_new


class FloodFill(Transform):
    r"""Rotates the images by a given angle (in degrees, counterclockwise)."""

    def __init__(self, truth_threshold, similarity_threshold):
        super().__init__()
        self.truth_threshold = truth_threshold
        self.sim_threshold = similarity_threshold

    def __call__(self, data):
        # unpack data
        X, Y = data

        def transform(X, Y):  # pylint: disable=missing-docstring
            Y_new = []
            for k, x in enumerate(X):
                original_image = x.copy()

                h, w = x.shape[:2]
                mask = np.zeros((h + 2, w + 2), np.uint8)

                # flood fill from every point in the ground image as seed point
                for i in range(Y[k].shape[0]):
                    for j in range(Y[k].shape[1]):
                        val = Y[k][i][j]
                        if val > self.truth_threshold:
                            cv2.floodFill(
                                    x,
                                    mask,
                                    seedPoint=(j, i),
                                    loDiff=(self.sim_threshold,
                                            self.sim_threshold,
                                            self.sim_threshold),
                                    upDiff=(self.sim_threshold,
                                            self.sim_threshold,
                                            self.sim_threshold),
                                    newVal=(int(val), int(val), int(val)))

                # reconstruct the groundtruth by comparing the new image
                # with the original and selecting the altered pixels
                for i in range(original_image.shape[0]):
                    for j in range(original_image.shape[1]):
                        if np.array_equal(x[i][j], original_image[i][j]):
                            x[i][j] = (0, 0, 0)

                Y_new.append(cv2.cvtColor(x, cv2.COLOR_RGB2GRAY))

            return Y_new

        # transform the ground truth masks
        Y_new = transform(X, Y)

        return X, Y_new


class Flip(Transform):
    r"""Flips an image with respect to a specified axis of reference."""

    def __init__(self, which):
        super().__init__()
        self.which = which

    def __call__(self, data):
        # unpack data
        X, Y = data

        def transform(Z, which):  # pylint: disable=missing-docstring
            Z_new = []

            for z in Z:
                z_new = z.copy()

                if which == "horizontal_flip":
                    z_new = z_new[:, ::-1]
                elif which == "vertical_flip":
                    z_new = z_new[::-1, :]
                elif which == "central_flip":
                    z_new = z_new[::-1, ::-1]
                else:
                    assert False, 'Type of flip not suported'

                Z_new.append(z_new)

            return np.array(Z_new)

        # transform the image data
        X_new = transform(X, self.which)
        # transform the ground truth masks
        Y_new = transform(Y, self.which)

        return X_new, Y_new


###############################################################################
## IMAGE FILTERING CLOSURES
###############################################################################


class GammaFilter(Transform):
    r"""Apply gamma filter
        if gamma=1, the image remains the same
        if gamma<1, the image gets darker
        if gamma>1, the image gets lighter

        Gamma can be a range (choose a random value from that range)
        or a float (it will use the same value for every image)
    """

    def __init__(self, gamma):
        super().__init__()
        self.gamma = gamma

    def __call__(self, data):
        # unpack data
        X, Y = data

        def adjust_gamma(image, gamma):  # pylint: disable=missing-docstring
            # build a lookup table mapping the pixel values [0, 255] to
            # their adjusted gamma values
            invGamma = 1.0 / gamma
            table = np.array([((i / 255.0)**invGamma) * 255
                              for i in np.arange(0, 256)]).astype("uint8")

            # apply gamma correction using the lookup table
            return cv2.LUT(image, table)

        def transform(Z):  # pylint: disable=missing-docstring
            Z_new = []

            if is_float(self.gamma):
                gammas = [self.gamma] * Z.shape[0]
            else:
                gammas = np.random.uniform(
                        low=self.gamma[0],
                        high=self.gamma[1],
                        size=(Z.shape[0], ))
            for i, z in enumerate(Z):
                gamma = gammas[i]
                z_new = adjust_gamma(z, gamma)
                Z_new.append(z_new)

            return np.array(Z_new)

        # filter the image data
        X_new = transform(X)
        # DON'T filter the ground truth masks

        return X_new, Y


class BilateralFilter(Transform):
    r"""It can reduce unwanted noise very well while keeping edges fairly
    sharp. However, it is very slow compared to most filters.

    See this: http://homepages.inf.ed.ac.uk/rbf/CVonline/LOCAL_COPIES/MANDUCHI1/Bilateral_Filtering.html
    """

    def __init__(self, sigma, count):
        r"""Constructs this filter.

        Arguments
        ---------
        sigma: int
            If they are small (less than 10), the filter will not have much
            effect, whereas if they are large (greater than 150), they will
            have a very strong effect, making they image look "cartoonish".
        count: int
            The filter will be applied `count` times
        """
        super().__init__()
        self.sigma = sigma
        self.count = count

    def __call__(self, data):
        # unpack data
        X, Y = data

        def transform(Z):  # pylint: disable=missing-docstring
            Z_new = []

            for z in Z:
                z_new = np.copy(z)
                for _ in range(self.count):
                    z_new = cv2.bilateralFilter(z_new, 10, self.sigma,
                                                self.sigma)
                Z_new.append(z_new)

            return np.array(Z_new)

        # filter the image data
        X_new = transform(X)
        # DON'T filter the ground truth masks

        return X_new, Y


class EliminateSpotsFilter(Transform):
    r"""It can be used to reduce small spots from predictions. See more at
    https://stackoverflow.com/a/30380543.

    Right now we tested it in preprocessing, with input the predictions. This
    has to be changed if it is used in a later stage of the pipeline

    Usage in config:

    .. code-block:: yaml

        - object:
            name: junction.augment.filters.EliminateSpotsFilter
            params:
              closing: 15
              opening: 10
              count: 3
    """

    def __init__(self, closing=15, opening=10, count=3):
        super().__init__()
        self.count = count
        self.closing = closing
        self.opening = opening

    def __call__(self, data):
        # unpack data
        X, Y = data

        def transform(Z):  # pylint: disable=missing-docstring
            Z_new = []

            for z in Z:
                z_new = np.copy(z)

                if len(z_new.shape) > 2 and z_new.shape[0] == 1 and \
                        np.max(z_new) == 1:
                    z_new = np.squeeze(z_new, axis=0)
                    z_new = 255 * z_new
                    z_new = z_new.astype(np.uint8)

                for _ in range(self.count):
                    img_bw = np.copy(z_new)
                    se1 = cv2.getStructuringElement(
                            cv2.MORPH_RECT, (self.closing, self.closing))
                    se2 = cv2.getStructuringElement(
                            cv2.MORPH_RECT, (self.opening, self.opening))
                    mask = cv2.morphologyEx(img_bw, cv2.MORPH_CLOSE, se1)
                    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, se2)

                    mask = mask / 255
                    out = z_new * mask

                    z_new = out

                Z_new.append(z_new)

            return np.array(Z_new)

        # filter the image data
        X_new = transform(X)
        # DON'T filter the ground truth masks

        return X_new, Y
