r"""This module contains various hooks that can be run post-inference."""

import numpy as np
from torchvision.transforms import (Compose as TorchvisionCompose, Normalize,
                                    ToTensor)

from ..augment.transforms import (ComposeTorchvision, Crop, MirrorPadDivisible)
from ..utils import TRAIN_DATA_SIZE


class ToSubmissionFormat(object):
    r"""Receives predictions as a matrix with elements between 0 and 1 and
    outputs a submission file.
    """

    def __init__(self, threshold, patch_size=16):
        self.threshold = threshold
        self.patch_size = patch_size

    def __call__(self, predictions):
        predictions = np.squeeze(predictions)

        submission = {}
        for index, y in enumerate(predictions):
            submission[index] = []
            for j in range(0, y.shape[1], self.patch_size):
                for i in range(0, y.shape[0], self.patch_size):
                    patch = y[i:(i + self.patch_size), j:(j + self.patch_size)]
                    label = self._get_label(patch)
                    submission[index].append((j, i, label))

        return submission

    def _get_label(self, y_patch):
        if self.threshold is not None:
            return int(np.mean(y_patch) > self.threshold)

        return np.mean(y_patch)


class ToClassLabels(object):
    r"""Receives predictions as a matrix with elements between 0 and 1 and
    outputs class labels.
    """

    def __init__(self, threshold):
        self.threshold = threshold

    def __call__(self, predictions):
        indices = predictions > self.threshold
        predictions = np.zeros(predictions.shape, dtype=np.int8)
        predictions[indices] = 1

        return predictions


class TrainPreInferenceHook(TorchvisionCompose):
    r"""A hook used to prepare the raw train data for inference."""

    def __init__(self):
        super().__init__(transforms=[
                MirrorPadDivisible(div=32),
                ComposeTorchvision(data_transforms=[
                        ToTensor(),
                        Normalize(
                                mean=[0.485, 0.456, 0.406],
                                std=[0.229, 0.224, 0.225]),
                ]),
                lambda X_y: X_y[0],
        ])


class TrainPostInferenceHook(TorchvisionCompose):
    r"""A post-inference hook used when predicting on the training data set."""

    def __init__(self):
        super().__init__(transforms=[
                lambda X: (np.squeeze(X), None),
                Crop(new_height=TRAIN_DATA_SIZE[0],
                     new_width=TRAIN_DATA_SIZE[1]),
                lambda X_y: X_y[0],
        ])
