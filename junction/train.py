r"""This module defines the class `Trainer` which is the main entry point to
run the training of our road segmentation models.
"""
import json
import os
import tempfile

from absl import logging
import numpy as np
from tensorboardX import SummaryWriter
import torch
from torch import nn
from torch.autograd import Variable
from torch.nn import functional as F
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader

from .augment.transforms import MirrorPadDivisible
from .utils import check_mkdir, default_optimizer, evaluate, fast_hist


class TrainingEngine(object):
    r"""A training closure which separates its setup from actually running it.
    Even though it can be quite generic, it makes use of things specific to our
    use case, such as the evaluation function :py:`junction.utils.evaluate`.
    """

    def __init__(self,
                 model,
                 optimizer_fn=default_optimizer,
                 criterion=None,
                 num_epochs=200,
                 batch_size=16,
                 save_every_epochs=None,
                 val_every_epochs=10,
                 class_threshold=0.3,
                 plateau_patience=20,
                 plateau_min_lr=1e-10,
                 save_dir=None,
                 snapshot_path=None):
        r"""Initializes this training session.

        Arguments
        ---------
        val_every_epochs: int
            Once every this number of epochs a validation step is performed.
        class_threshold: float
            This should be a number between 0 and 1 specifying how to threshold
            the model predictions in order to get binary class labels.
        plateau_patience: int
            If the validation loss does not get better this many number of
            epochs, the learning rate is decayed according to
            `torch.optim.lr_schedule.ReduceLROnPlateau`.
            NOTE: This number must be relative to `num_epochs`, not to
            `plateau_patience`.  We scale it accordingly.
        optimizer: closure, None
            A closure which accepts the model parameters and returns the torch
            optimizer to use.
        save_dir: str
            The directory used to save snapshots and other temporary files for
            this training session. If None, it creates a temporary directory.
            NOTE: If provided, it expects it to be created already.
        """
        self.model = model
        self.optimizer = optimizer_fn(self.model.parameters())
        self.criterion = criterion
        self.num_epochs = num_epochs
        self.batch_size = batch_size
        self.save_every_epochs = save_every_epochs
        self.val_every_epochs = val_every_epochs
        self.class_threshold = class_threshold
        self.plateau_patience = plateau_patience
        self.plateau_min_lr = plateau_min_lr
        self.save_dir = save_dir
        self.snapshot_path = snapshot_path

        # the tensorboard summary writer
        self.writer = SummaryWriter(self.save_dir)

        # default criterion is binary cross-entropy
        if self.criterion is None:
            self.criterion = nn.BCELoss()

        # the default model saving frequency: once at the end
        if self.save_every_epochs is None:
            self.save_every_epochs = self.num_epochs

        # set up the default save dir if not specified
        if self.save_dir is None:
            self.save_dir = tempfile.gettempdir()
            check_mkdir(self.save_dir)

        # prepare function to get per-patch scores which is what we are
        # evaluated on
        self.patch_avg = torch.nn.AvgPool2d(kernel_size=16)

        # keep track of the mirrored margin offsets
        # FIXME(ccruceru): Find a more elegant method to do this; not very nice
        # to assume that this transformer will always be used.
        self.paddings = MirrorPadDivisible.last_paddings

    def __call__(self, train_dataset, val_dataset):
        r"""Runs this training session.

        Arguments
        ---------
        train_dataset: :class:`torch.utils.data.DataSet`
            The training dataset.
        val_dataset: :class:`torch.utils.data.DataSet`
            The validation dataset.
        """
        # initialize the training by possibly loading from a given snapshot
        best_record = self._init_train()

        # define the training/validation data loaders
        train_loader = DataLoader(
                train_dataset,
                batch_size=self.batch_size,
                num_workers=20,
                shuffle=True)
        val_loader = DataLoader(
                val_dataset,
                batch_size=self.batch_size,
                num_workers=20,
                shuffle=False)
        # wrap the optimizer in a LR scheduler
        scheduler = ReduceLROnPlateau(
                self.optimizer,
                mode='min',  # NOTE: only use "loss" objectives
                patience=(self.plateau_patience / self.val_every_epochs),
                min_lr=self.plateau_min_lr)

        # run the actual training
        for epoch in range(best_record['epoch'], self.num_epochs + 1):
            self._train(train_loader, epoch)
            if epoch % self.val_every_epochs == 0:
                val_loss = self._validate(val_loader, epoch, best_record)
                scheduler.step(val_loss)

            # save the model from time to time
            if epoch % self.save_every_epochs == 0:
                torch.save(
                        self.model.state_dict(),
                        os.path.join(self.save_dir, 'net_{}.pth'.format(epoch)))

    def _init_train(self):
        if self.snapshot_path is not None:
            logging.info('Training resumes from %s', self.snapshot_path)

            # unpack the best record to recover from
            with open(
                    os.path.join(self.snapshot_path, 'best_record.json'),
                    'r') as infile:
                best_record = json.load(infile)

            # load the network state and the optimizer state
            self.model.load_state_dict(
                    torch.load(os.path.join(self.snapshot_path, 'net.pth')))
            self.optimizer.load_state_dict(
                    torch.load(os.path.join(self.snapshot_path, 'opt.pth')))
            self.optimizer.param_groups[0]['lr'] = best_record['lr']
        else:
            # we only need the current epoch and the best f1_score to be
            # initialized
            best_record = {'epoch': 1, 'f1_score': 0}

        # place model on CUDA if available
        if torch.cuda.is_available():
            logging.debug('Using CUDA for training')
            self.model = self.model.cuda()
            # maybe make the model data-parallel
            self.model = nn.DataParallel(self.model)

        return best_record

    def _train(self, train_loader, epoch):
        # enter train mode
        self.model.train()

        # current overall iteration
        global_step = (epoch - 1) * len(train_loader)

        # go through each batch
        for data in train_loader:
            # load current data
            X, y = data
            X, y = Variable(X), Variable(y)
            # place them on CUDA if available
            if torch.cuda.is_available():
                X = X.cuda()  # pylint: disable=no-member
                y = y.cuda()  # pylint: disable=no-member

            # do the optimization step
            self.optimizer.zero_grad()
            y_pred = self.model(X)
            loss = self.criterion(self._crop_predictions(y_pred), y)
            loss.backward()
            self.optimizer.step()

            # increment the global step
            global_step += 1

            # log the current loss
            self.writer.add_scalar('train_loss', loss, global_step)

        # print the status at the end of the epoch
        logging.debug('epoch %d, train loss %.5f', epoch, loss)

    def _validate(self, val_loader, epoch, best_record):
        # enter eval mode
        self.model.eval()

        # update in an online manner the confusion matrix (`hist` below) and
        # cumulated losses
        n_points = 0
        loss_sum = 0
        loss_patch_sum = 0
        hist = np.zeros((2, 2))

        # go through each batch
        for data in val_loader:
            # this makes sure that the gradients memory is spared during
            # inference
            with torch.no_grad():
                # load current data
                X, y = data
                X = Variable(X)
                y = Variable(y)
                # place them on CUDA if available
                if torch.cuda.is_available():
                    X = X.cuda()  # pylint: disable=no-member
                    y = y.cuda()  # pylint: disable=no-member

                # get predictions
                y_pred = self.model(X)
                y_pred = self._crop_predictions(y_pred)  # crop them now!
                y_pred_probabs = F.sigmoid(y_pred)

                # losses
                loss = self.criterion(y_pred, y)
                loss_patch = self.criterion(
                        self.patch_avg(y_pred), self.patch_avg(y))

            # update loss
            loss_sum += float(loss)
            loss_patch_sum += float(loss_patch)
            n_points += X.size(0)

            # get the predictions as class labels
            y = self._threshold_predictions(y.data.cpu().numpy().flatten())
            y_pred_binary = self._threshold_predictions(
                    y_pred_probabs.data.cpu().numpy().flatten())
            # update the confusion matrix
            hist += fast_hist(y_pred_binary, y)

        # validation loss is the mean loss across batches
        val_loss = loss_sum / n_points
        val_patch_loss = loss_patch_sum / n_points
        # get the values of the metrics
        acc, acc_cls, mean_iu, fwavacc, precision, recall, f1_score = \
                evaluate(hist=hist)

        # check if we reached a better state
        if f1_score > best_record['f1_score']:
            # keep track of it
            best_record['epoch'] = epoch
            best_record['val_loss'] = val_loss
            best_record['val_patch_loss'] = val_patch_loss
            best_record['acc'] = acc
            best_record['acc_cls'] = acc_cls
            best_record['mean_iu'] = mean_iu
            best_record['fwavacc'] = fwavacc
            best_record['precision'] = precision
            best_record['recall'] = recall
            best_record['f1_score'] = f1_score
            best_record['lr'] = self.optimizer.param_groups[0]['lr']

            # also persist it
            with open(os.path.join(self.save_dir, 'best_record.json'), 'w') \
                    as outfile:
                json.dump(best_record, outfile, indent=2)
            # also overwrite the best model so far
            torch.save(self.model.state_dict(),
                       os.path.join(self.save_dir, 'net_br.pth'))
            torch.save(self.optimizer.state_dict(),
                       os.path.join(self.save_dir, 'opt_br.pth'))

            # print the corresponding f1 score to console too
            logging.debug('New best record: f1_score (%.5f)', f1_score)

        # add them to tensorboard
        self.writer.add_scalar('val_loss', val_loss, epoch)
        self.writer.add_scalar('val_patch_loss', val_patch_loss, epoch)
        self.writer.add_scalar('acc', acc, epoch)
        self.writer.add_scalar('acc_cls', acc_cls, epoch)
        self.writer.add_scalar('mean_iu', mean_iu, epoch)
        self.writer.add_scalar('fwavacc', fwavacc, epoch)
        self.writer.add_scalar('precision', precision, epoch)
        self.writer.add_scalar('recall', recall, epoch)
        self.writer.add_scalar('f1_score', f1_score, epoch)
        # also keep track of the learning rate as it might change
        self.writer.add_scalar('learning_rate',
                               self.optimizer.param_groups[0]['lr'], epoch)

        # go back to train mode
        self.model.train()

        return val_loss  # NOTE: We return the overall loss.

    def _threshold_predictions(self, predictions):
        indices = predictions > self.class_threshold
        predictions = np.zeros(predictions.shape, dtype=np.int8)
        predictions[indices] = 1

        return predictions

    def _crop_predictions(self, predictions):
        # the network will always output mirrored margins; we check if cropping
        # is needed if the margin offsets have been saved in the corresponding
        # transformer
        if self.paddings is None:
            return predictions

        y_min, y_max, x_min, x_max = self.paddings
        _, _, h, w = predictions.shape

        return predictions[:, :, y_min:(h - y_max), x_min:(w - x_max)]
