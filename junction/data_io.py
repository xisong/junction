r"""This module defines the :py:class:`CILRoadSegmentationDataset` class and
various utility functions for loading raw and pre-processed data.
"""
# They are in order but pylint does not recognize cv2!
# pylint: disable=wrong-import-order
import glob
import os

import cv2
import numpy as np
import torch

from .utils import torch_shuffle, check_mkdir


class CILRoadSegmentationDataset(torch.utils.data.Dataset):
    r"""A simple wrapper over input data/labels for the CIL Road Segmentation
    dataset.
    """

    def __init__(self, X, y=None, shuffle=False):
        r"""Initialzes this dataset."""
        # shuffle them if required
        if shuffle:
            X, y = torch_shuffle(X, y)

        self.X = X
        self.y = y

    def __getitem__(self, index):
        r"""Returns the data point (i.e. image, or image patch) and its mask
        (i.e. binary matrix) at the given index. The latter is optional.
        """
        if self.y is None:
            return self.X[index]

        return self.X[index], self.y[index]

    def __len__(self):
        r"""Returns the length of this dataset."""
        return len(self.X)


def load_raw_data(path, which):
    r"""Loads the raw CIL Road Segmentation data set.  The second argument is
    one of ['preprocessing', 'training', 'inference'], and specifies which
    dataset should be loaded.

    If which is 'inference', then the ordered identifiers of the raw images is
    returned too.
    """

    def read_images(path, groundtruth=False):  # pylint: disable=missing-docstring
        images = []
        ids = []
        for image_path in glob.glob(os.path.join(path, '*.png')):
            image_arr = cv2.imread(image_path)
            if groundtruth:
                image_arr = cv2.cvtColor(image_arr, cv2.COLOR_BGR2GRAY)
            else:
                image_arr = cv2.cvtColor(image_arr, cv2.COLOR_BGR2RGB)
            images.append(image_arr)
            ids.append(int(os.path.basename(image_path).split('.')[0]))

        return np.array(images), np.array(ids, dtype=np.int16)

    # read the training data
    X, X_ids = read_images(os.path.join(path, 'training', 'images'))
    y, y_ids = read_images(
            os.path.join(path, 'training', 'groundtruth'), groundtruth=True)

    # early-return for training/preprocessing which don't need test data
    if which in ['training', 'preprocessing']:
        return (X, y), (X_ids, y_ids)

    # test data
    X_test, X_test_ids = read_images(os.path.join(path, 'testing'))

    return (X, y, X_test), (X_ids, y_ids, X_test_ids)


def load_preprocessed_data(path, which):
    r"""Loads a pre-processed CIL Road Segmentation data set.  The second
    argument is one of ['preprocessing', 'training'], and specifies which
    dataset should be loaded.
    """
    if which not in ['preprocessing', 'training']:
        raise ValueError('Loading pre-processed data for inference is not '
                         'supported!')

    X = np.load(os.path.join(path, 'training', 'X.npy'))
    y = np.load(os.path.join(path, 'training', 'y.npy'))

    return X, y


def dump_preprocessed_data(data, path):
    r"""Writes to disk a pre-processed version of the CIL Road Segmentation
    data set.

     ├── testing
     │   ├── [EMPTY]: currently in postprocessing we just augment training data
     └── training
         ├── X.npy
         └── y.npy

    NOTE: This function should be consistent with
    :py:`~.load_preprocessed_data()` in terms of convention for pre-processed
    data. In other words, that one should be able to successfully load the data
    dumped by this one.
    """
    training_dir = check_mkdir(os.path.join(path, 'training'), increment=False)
    _ = check_mkdir(os.path.join(path, 'testing'), increment=False)

    np.save(os.path.join(training_dir, 'X.npy'), data[0])
    np.save(os.path.join(training_dir, 'y.npy'), data[1])


def dump_predictions(predictions, path, ids, file_name=None,
                     ids_file_name=None):
    r"""Writes to disk the predictions resulted by running a successful
    'inference' action in our framework.

    It can write it either as a numpy array, for latter post-processing, or as
    a submission file as expected on kaggle, depending on the type of
    `predictions`: either numpy array or dict mapping patches to labels,
    respectively.
    """
    if isinstance(predictions, np.ndarray):
        if file_name is None:
            file_name = 'y_pred.npy'
        np.save(os.path.join(path, file_name), predictions)
    else:
        assert isinstance(predictions, dict)
        if file_name is None:
            file_name = 'submission.csv'
        with open(os.path.join(path, file_name), 'w') as sfile:
            sfile.write('id,prediction\n')
            for index, patched_labels in predictions.items():
                sfile.writelines(
                        '{:03d}_{}_{},{}\n'.format(ids[index], j, i, label)
                        for j, i, label in patched_labels)

    if ids_file_name is not None:
        dump_ids(ids, os.path.join(path, ids_file_name))


def dump_ids(ids, path):
    r"""Save an array of raw image identifiers to a given file."""
    np.savetxt(path, ids.astype(int), fmt='%d')
