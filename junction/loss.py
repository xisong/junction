r"""This module contains objective functions to minimize during training.

NOTE: The convention is that the predictions are the logits returned directly
by the models. In other words, they should not be "squashed" in advance. The
motivation is that certain loss functions are more numerically stable in this
case.
"""

import torch
from torch.nn import functional as F


class Sum(object):
    r"""A closure which sums multiple loss functions."""

    def __init__(self, loss_fns, weights=None):
        self.loss_fns = loss_fns
        self.weights = weights

        if self.weights is None:
            self.weights = torch.ones(len(loss_fns))

    def __call__(self, y_pred, y):
        total_loss = 0
        for loss_fn, weight in zip(self.loss_fns, self.weights):
            total_loss += weight * loss_fn(y_pred, y)

        return total_loss


class DiceLoss(object):
    r"""The smoothed Jaccard index loss."""

    def __init__(self, smooth=1.0):
        self.smooth = smooth

    def __call__(self, y_pred, y):
        # apply sigmoid
        y_pred = F.sigmoid(y_pred)

        # flatten all other dimensions
        num = y_pred.size(0)
        y_pred = y_pred.view(num, -1)
        y = y.view(num, -1)

        # apply weights
        intersection = (y_pred * y).sum(1)
        scores = 2. * (intersection + self.smooth) / (
                y_pred.sum(1) + y.sum(1) + self.smooth)

        return 1 - torch.clamp(scores.sum() / num, 0.0, 1.0)
