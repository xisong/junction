r"""This model defines the U-Net models as used in the TernausNet paper
:cite:`iglovikov2018ternausnet`.  The corresponding LICENSE is attached below.


MIT License

Copyright (c) 2017 Vladimir Iglovikov, Alexey Shvets

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# The `*Decoder` classes are meant to be abstract, so this warning is useless.
# pylint: disable=abstract-method
from absl import logging
import torch
from torch import nn
import torchvision
from torchvision import models

try:
    from inplace_abn.models.wider_resnet import net_wider_resnet38 as WiderResNet38
    from inplace_abn.imagenet.utils import get_model_params
except ImportError:
    pass

###############################################################################
## Main U-Net models
###############################################################################


class DecoderBase(nn.Module):
    r"""A base class for modules which include a decoder (FCNs).

    It has several attributes which are used through all decoders.
    """

    def __init__(self, deconv):
        r"""Initializes this base decoder module.

        Arguments
        --------
        deconv: str
            Sets up the type of deconvolution used.  See the documentation of
            :py:`DecoderBlock.__init__` for possible values.
        """
        super().__init__()

        # we always have the 3 RGB channels at input
        self.num_input_channels = 3

        # hardcoded value specific to all architectures considered below; its
        # double corresponds to the number of channels after the first
        # convolutional layer, as well as the filter size in the last layer of
        # the decoder
        self.num_filters = 32

        # as mentioned above, we always have 64 filters after the first
        # convolutional layer
        self.num_filters_conv1_out = 64

        # closure which forwards the `deconv` parameter to `DecoderBlock`
        self.make_decoder_block = lambda n_in, n_middle, n_out: \
                DecoderBlock(n_in, n_middle, n_out, deconv)


class VGGDecoder(DecoderBase):
    r"""Base class for the U-Net architectures that use VGG nets as their
    encoder.
    """

    def __init__(self, deconv):
        super().__init__(deconv=deconv)

        # bottleneck part separating encoder from decoder
        self.center = self.make_decoder_block(
                self.num_filters * 8 * 2,  # input
                self.num_filters * 8 * 2,  # middle
                self.num_filters * 8  # output
        )

        # the decoder obtained by chainning multiple decoder blocks
        self.dec5 = self.make_decoder_block(
                self.num_filters * (16 + 8),  # input
                self.num_filters * 8 * 2,  # middle
                self.num_filters * 8  # output
        )
        self.dec4 = self.make_decoder_block(
                self.num_filters * (16 + 8),  # input
                self.num_filters * 8 * 2,  # middle
                self.num_filters * 4  # output
        )
        self.dec3 = self.make_decoder_block(
                self.num_filters * (8 + 4),  # input
                self.num_filters * 4 * 2,  # middle
                self.num_filters * 2  # output
        )
        self.dec2 = self.make_decoder_block(
                self.num_filters * (4 + 2),  # input
                self.num_filters * 2 * 2,  # middle
                self.num_filters  # output
        )
        self.dec1 = ConvRelu3x3(self.num_filters * (2 + 1), self.num_filters)
        self.final = nn.Conv2d(self.num_filters, 1, kernel_size=1)


class UNet11(VGGDecoder):
    r"""The main U-Net architecture which is also presented in the TernausNet
    paper :cite:`iglovikov2018ternausnet`, using the VGG1 net where the fully
    connected layers are replaced by another so-called "bottleneck"
    convolutional layer.

    It can use the pretrained VGG11 to initialize its encoder.
    """

    def __init__(self, pretrained=False, deconv='standard'):
        super().__init__(deconv=deconv)

        # the encoder part obtained from a (possibly) pre-trained VGG11
        enc = models.vgg11(pretrained=pretrained).features
        relu = nn.ReLU(inplace=True)
        self.conv1 = nn.Sequential(enc[0], relu)
        self.conv2 = nn.Sequential(enc[3], relu)
        self.conv3 = nn.Sequential(enc[6], relu, enc[8], relu)
        self.conv4 = nn.Sequential(enc[11], relu, enc[13], relu)
        self.conv5 = nn.Sequential(enc[16], relu, enc[18], relu)

        # max-pool layer used after each one-two convolution(s)
        self.pool = nn.MaxPool2d(2, 2)

    def forward(self, x):  # pylint: disable=arguments-differ
        # run encoder
        conv1 = self.conv1(x)
        conv2 = self.conv2(self.pool(conv1))
        conv3 = self.conv3(self.pool(conv2))
        conv4 = self.conv4(self.pool(conv3))
        conv5 = self.conv5(self.pool(conv4))

        # bottleneck layer
        center = self.center(self.pool(conv5))

        # run decoder
        dec5 = self.dec5(torch.cat([center, conv5], 1))
        dec4 = self.dec4(torch.cat([dec5, conv4], 1))
        dec3 = self.dec3(torch.cat([dec4, conv3], 1))
        dec2 = self.dec2(torch.cat([dec3, conv2], 1))
        dec1 = self.dec1(torch.cat([dec2, conv1], 1))

        return self.final(dec1)


class UNet16(VGGDecoder):
    r"""Similar to :py:class:`UNet11` but using VGG16 instead of VGG11 as its
    encoder architecture.

    It can use the pretrained VGG16 network from :py:module:`torchvision` to
    initialize its encoder.
    """

    def __init__(self, pretrained=False, deconv='bilinear'):
        super().__init__(deconv=deconv)

        # the encoder part obtained from a (possibly) pre-trained VGG16
        enc = torchvision.models.vgg16(pretrained=pretrained).features
        relu = nn.ReLU(inplace=True)
        self.conv1 = nn.Sequential(enc[0], relu, enc[2], relu)
        self.conv2 = nn.Sequential(enc[5], relu, enc[7], relu)
        self.conv3 = nn.Sequential(enc[10], relu, enc[12], relu, enc[14], relu)
        self.conv4 = nn.Sequential(enc[17], relu, enc[19], relu, enc[21], relu)
        self.conv5 = nn.Sequential(enc[24], relu, enc[26], relu, enc[28], relu)

        # max-pool layer used after each one-two convolution(s)
        self.pool = nn.MaxPool2d(2, 2)

    def forward(self, x):  # pylint: disable=arguments-differ
        # run encoder
        conv1 = self.conv1(x)
        conv2 = self.conv2(self.pool(conv1))
        conv3 = self.conv3(self.pool(conv2))
        conv4 = self.conv4(self.pool(conv3))
        conv5 = self.conv5(self.pool(conv4))

        # bottleneck layer
        center = self.center(self.pool(conv5))

        # run decoder
        dec5 = self.dec5(torch.cat([center, conv5], 1))
        dec4 = self.dec4(torch.cat([dec5, conv4], 1))
        dec3 = self.dec3(torch.cat([dec4, conv3], 1))
        dec2 = self.dec2(torch.cat([dec3, conv2], 1))
        dec1 = self.dec1(torch.cat([dec2, conv1], 1))

        return self.final(dec1)


class ResNet34Decoder(DecoderBase):
    r"""Base class for the U-Net architecture that uses ResNet34 as its
    encoder.
    """

    def __init__(self, deconv):
        super().__init__(deconv=deconv)

        # bottleneck part separating encoder from decoder
        self.center = self.make_decoder_block(
                self.num_filters * 8 * 2,  # input
                self.num_filters * 8 * 2,  # middle
                self.num_filters * 8  # output
        )

        # the decoder obtained by chainning multiple decoder blocks
        self.dec5 = self.make_decoder_block(
                self.num_filters * (16 + 8),  # input
                self.num_filters * 8 * 2,  # middle
                self.num_filters * 8  # output
        )
        self.dec4 = self.make_decoder_block(
                self.num_filters * (8 + 8),  # input
                self.num_filters * 8 * 2,  # middle
                self.num_filters * 8  # output
        )
        self.dec3 = self.make_decoder_block(
                self.num_filters * (8 + 4),  # input
                self.num_filters * 4 * 2,  # middle
                self.num_filters * 2  # output
        )
        self.dec2 = self.make_decoder_block(
                self.num_filters * (2 + 2),  # input
                self.num_filters * 2 * 2,  # middle
                self.num_filters * 2 * 2  # output
        )
        self.dec1 = self.make_decoder_block(
                self.num_filters * 2 * 2,  # input
                self.num_filters * 2 * 2,  # middle
                self.num_filters  # output
        )
        self.dec0 = ConvRelu3x3(self.num_filters, self.num_filters)
        self.final = nn.Conv2d(self.num_filters, 1, kernel_size=1)


class UNet34(ResNet34Decoder):
    r"""Similar to :py:class:`UNet11, UNet16` but using ResNet34 as its encoder
    architecture.

    It can use the pretrained ResNet34 network from :py:module:`torchvision` to
    initialize its encoder.

    NOTE: Even though it has several layers on the expansive path with a number of
    filters equal to 32, this network requires the resolution of the input to
    be dividable by 64!
    """

    def __init__(self, pretrained=False, deconv='bilinear'):
        super().__init__(deconv=deconv)

        # 2x2 pooling layer
        self.pool = nn.MaxPool2d(2, 2)

        # the encoder part obtained from a (possibly) pre-trained ResNet34
        enc = torchvision.models.resnet34(pretrained=pretrained)
        self.conv1 = nn.Sequential(enc.conv1, enc.bn1, enc.relu, self.pool)
        self.conv2 = enc.layer1
        self.conv3 = enc.layer2
        self.conv4 = enc.layer3
        self.conv5 = enc.layer4

    def forward(self, x):  # pylint: disable=arguments-differ
        # run encoder
        conv1 = self.conv1(x)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        conv4 = self.conv4(conv3)
        conv5 = self.conv5(conv4)

        # bottleneck layer
        center = self.center(self.pool(conv5))

        # run decoder
        dec5 = self.dec5(torch.cat([center, conv5], 1))
        dec4 = self.dec4(torch.cat([dec5, conv4], 1))
        dec3 = self.dec3(torch.cat([dec4, conv3], 1))
        dec2 = self.dec2(torch.cat([dec3, conv2], 1))
        dec1 = self.dec1(dec2)
        dec0 = self.dec0(dec1)

        return self.final(dec0)


class WiderResNet38Decoder(DecoderBase):
    r"""Base class for the U-Net architecture that uses WiderResNet38 as its
    encoder.
    """

    def __init__(self, deconv):
        super().__init__(deconv=deconv)

        # bottleneck part separating encoder from decoder
        self.center = self.make_decoder_block(
                1024,  # input
                self.num_filters * 8,  # middle
                self.num_filters * 8,  # output
        )
        self.dec5 = self.make_decoder_block(
                1024 + self.num_filters * 8,  # input
                self.num_filters * 8,  # middle
                self.num_filters * 8,  # output
        )
        self.dec4 = self.make_decoder_block(
                512 + self.num_filters * 8,  # input
                self.num_filters * 8,  # middle
                self.num_filters * 8,  # output
        )
        self.dec3 = self.make_decoder_block(
                256 + self.num_filters * 8,  # input
                self.num_filters * 2,  # middle
                self.num_filters * 2,  # output
        )
        self.dec2 = self.make_decoder_block(
                128 + self.num_filters * 2,  # input
                self.num_filters * 2,  # middle
                self.num_filters,  # output
        )
        self.dec1 = ConvRelu3x3(64 + self.num_filters, self.num_filters)
        self.final = nn.Conv2d(self.num_filters, 1, kernel_size=1)


class UNet38(WiderResNet38Decoder):
    r"""U-Net architecture based on the WiderResNet38 architecture as its
    encoder.

    It uses the 'inplace_abn' framework for memory-efficient training,
    described in :cite:`rotabulo2017place`, to load the network. The idea
    originates from :cite:`2018arXiv180600844I`.
    """

    def __init__(self, snapshot=None, model_args=None, deconv='nearest'):
        r"""Initializes this model.

        Arguments
        ---------
        snapshot: str or None
            If this is given, then it should point to the snapshot of Wider
            ResNet38 model trained on ImageNet.
        model_args: dict or None
            If this is given, then it should be a dictionary of arguments to
            construct the Wider ResNet38 model as expected by the function
            `inplace_abn.imagenet.utils.get_model_params`. Not all of them have
            to be passed, just the ones which differ from the default from [1].

        NOTE: Overwritting default arguments from [1] should be done with care,
        especially when still loading the model trained on ImageNet. In
        general, the two arguments should be mutually exclusive: either use the
        default settings which are compatible with the model trained on
        ImageNet, or use new arguments and train from scratch.

        References
        ----------
        [1]: https://github.com/mapillary/inplace_abn/blob/master/experiments/wider_resnet38_ipabn_lr_256.json
        """
        super().__init__(deconv=deconv)

        # the default arguments used to create the model trained on ImageNet
        args = {
                "arch": "wider_resnet38",
                "activation": "leaky_relu",
                "leaky_relu_slope": 0.01,
                "input_3x3": True,
                "bn_mode": "inplace",
                "classes": 1000,
        }

        # update them if required
        if model_args is not None:
            args.update(model_args)

        # construct the model
        # NOTE: We wrap it in `torch.nn.DataParallel` and explicitly place it
        # on GPU as we already know that this model will only work on GPU, due
        # to the dependency on `inplace_abn`.
        wider_resnet38 = \
                nn.DataParallel(WiderResNet38(**get_model_params(args))).cuda()

        # construct the version trained on ImageNet if a snapshot file is
        # provided
        if snapshot is not None:
            if model_args is not None:
                logging.warning('This operation is dangerous: make sure the '
                                'trained model matches the given model args!')
            wider_resnet38.load_state_dict(torch.load(snapshot)['state_dict'])

        # encoder
        self.conv1 = nn.Conv2d(
                self.num_input_channels,
                self.num_filters_conv1_out,
                kernel_size=3,
                padding=1,
                bias=False)
        self.conv2 = wider_resnet38.module.mod2
        self.conv3 = wider_resnet38.module.mod3
        self.conv4 = wider_resnet38.module.mod4
        self.conv5 = wider_resnet38.module.mod5

        # 2x2 pooling layer
        self.pool = nn.MaxPool2d(2, 2)

    def forward(self, x):  # pylint: disable=arguments-differ
        # run encoder
        conv1 = self.conv1(x)
        conv2 = self.conv2(self.pool(conv1))
        conv3 = self.conv3(self.pool(conv2))
        conv4 = self.conv4(self.pool(conv3))
        conv5 = self.conv5(self.pool(conv4))

        # bottleneck layer
        center = self.center(self.pool(conv5))

        # run decoder
        dec5 = self.dec5(torch.cat([center, conv5], 1))
        dec4 = self.dec4(torch.cat([dec5, conv4], 1))
        dec3 = self.dec3(torch.cat([dec4, conv3], 1))
        dec2 = self.dec2(torch.cat([dec3, conv2], 1))
        dec1 = self.dec1(torch.cat([dec2, conv1], 1))

        return self.final(dec1)


###############################################################################
## Utility blocks
###############################################################################


class ConvRelu3x3(nn.Module):
    r"""A block consisting of a 3x3 convolutional layer followed by a ReLU
    activation.
    """

    def __init__(self, n_in, n_out):
        super().__init__()

        self.conv = nn.Conv2d(n_in, n_out, kernel_size=3, padding=1)
        self.activation = nn.ReLU(inplace=True)

    def forward(self, x):  # pylint: disable=arguments-differ
        return self.activation(self.conv(x))


class DecoderBlock(nn.Module):
    r"""A decoder block in a U-Net.

    The standard is to chain a 3x3 convolution + ReLU and a 3x3 transposed
    convolution (i.e. "deconvolution") + ReLU.  However, as the authors of this
    class (see :cite:`iglovikov2018ternausnet`) suggest, to avoid checkboard
    artifacts [1] we should better use either 4x4 deconvolutions or upsampling
    and following it by a convolution layer.

    NOTE: This essentially combined the classes `DecoderBlock` and
    `DecoderBlockV2` from the authors' code.

    [1]: https://distill.pub/2016/deconv-checkerboard/
    """

    def __init__(self, n_in, n_middle, n_out, deconv):
        r"""Initialize this decoder block.

        Arguments
        ---------
        deconv: str
            If it is either '3x3' or 'standard', then the 3x3 deconvolutions
            are used. If it is '4x4', then the 4x4 deconvoutions are used. If
            it is 'nearest' or 'bilinear', then the upsampling method is used,
            where the interpolation is done accordingly.
        """
        super().__init__()

        if deconv not in ['3x3', 'standard', '4x4', 'nearest', 'bilinear']:
            raise ValueError('Unknown value for parameter `deconv`: ', deconv)

        if deconv in ['3x3', 'standard', '4x4']:
            # '3x3' and 'standard' correspond to the same configuration
            kernel_size = 4 if deconv == '4x4' else 3
            # we need output padding for the standard setting
            output_padding = 0 if deconv == '4x4' else 1
            self.block = nn.Sequential(
                    ConvRelu3x3(n_in, n_middle),
                    nn.ConvTranspose2d(
                            n_middle,
                            n_out,
                            kernel_size=kernel_size,
                            stride=2,
                            padding=1,
                            output_padding=output_padding),
                    nn.ReLU(inplace=True),
            )
        else:
            self.block = nn.Sequential(
                    nn.Upsample(scale_factor=2, mode=deconv),
                    ConvRelu3x3(n_in, n_middle),
                    ConvRelu3x3(n_middle, n_out),
            )

    def forward(self, x):  # pylint: disable=arguments-differ
        return self.block(x)
