r"""This module defines the functionality needed to perform inference using one
of our pre-trained models.
"""
import os

from absl import logging
import numpy as np
import torch
from torch import nn
from torch.autograd import Variable
from torch.nn import functional as F
from torch.utils.data import DataLoader


class InferenceEngine(object):
    r"""The inference engine.  It is implemented as a closure which separates
    its setup from actually running it.
    """

    def __init__(self, model, snapshot_path, batch_size=32):
        r"""Initializes this inference engine.

        Arguments
        ---------
        snapshot_path: str
            Can be either the path to a directory where the model is expected
            to be saved as 'net.pth', or a path to the model itself.
        """
        self.model = model
        self.snapshot_path = snapshot_path
        self.batch_size = batch_size

        # check the snapshot_path
        if os.path.isdir(self.snapshot_path):
            self.snapshot_path = os.path.join(self.snapshot_path, 'net.pth')

    def __call__(self, test_dataset):
        r"""Runs this inference session.

        Arguments
        ---------
        test_dataset: :class:`torch.utils.data.DataSet`
            The test dataset.
        """
        # initialize this inference session
        self._init_inference()

        # data batch loader
        test_loader = DataLoader(
                test_dataset,
                batch_size=self.batch_size,
                num_workers=8,
                shuffle=False)

        # array holding the predictions
        Y_pred = []

        # don't compute gradients
        with torch.no_grad():
            for i, X in enumerate(test_loader):
                logging.debug('Running inference on batch %d/%d', i + 1,
                              len(test_loader))
                # prepare the batch
                X = Variable(X)

                # get prediction
                y_pred = self.model(X)
                y_pred = F.sigmoid(y_pred)  # squash them between 0 and 1

                # bring it on cpu and store it
                Y_pred.extend(list(y_pred.data.cpu().numpy()))

        # convert to numpy array
        return np.array(Y_pred)

    def _init_inference(self):
        logging.info('Inference resumes from %s', self.snapshot_path)

        # make the model data-parallel
        self.model = nn.DataParallel(self.model)

        # load params
        map_location = None if torch.cuda.is_available() else 'cpu'
        state_dict = torch.load(self.snapshot_path, map_location=map_location)
        self.model.load_state_dict(state_dict)

        # place it on cuda if available
        if torch.cuda.is_available():
            self.model = self.model.cuda()
