r"""Setup script for this project."""
from setuptools import setup

setup(
        name='junction',
        version='0.1',
        description=
        'A UNet-based implementation for road segmentation in satellite imagery',
        author='Mislav Balunović, Calin Cruceru, Andrei Isac, Andrei Ursache',
        author_email=
        'mbalunovic@ethz.ch, ccruceru@ethz.ch, isaca@ethz.ch, ura@ethz.ch',
        setup_requires=[
                'setuptools>=18.0',
                'cython',
                'pytest-runner',
        ],
        dependency_links=['https://github.com/calincru/inplace_abn'],
        install_requires=[
                'absl-py==0.2.2',
                'numpy==1.14',
                'scipy==1.1',
                'matplotlib==2.2',
                'jupyter==1.0',
                'ruamel.yaml==0.15.37',
                'torch==0.4',
                'torchvision==0.2',
                'tensorboard==1.8',
                'tensorboardX==1.2',
                'tensorflow==1.8',
                'opencv-python==3.4.1.15',
                'pillow==5.1.0',
                'pylint==1.9',
                'scikit-image==0.13.1',
                'yapf',
                'inplace_abn',
                'sphinxcontrib-bibtex',
                'sphinx_rtd_theme',
                'mock',
        ],
        tests_require=[
                'pytest',
        ],
        packages=[
                'junction', 'junction.augment', 'junction.models',
                'junction.postprocess'
        ],
        license='LICENSE.txt',
)
