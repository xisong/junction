r"""This script is the main entry-point for interacting with our models, be it
for data preprocessing, training, inference, or data transformation.

It works as follows:
    * receives as input a configuration file written in YAML with an expected
    format as described in 'example_config.yaml', located in the root of this
    repository.
    * parses the configuration file and runs the corresponding actions: data
    pre-processing, training, inference.

NOTE: The convention for composing transforms in this driver is that the
preprocessing transforms which are run separately or before a training session
are composed using the implementation from
`junction.augment.transforms.Compose`, while others, such as the pre- and
post-inference hooks are composed using normal composition. For the latter, we
use the implementation from `torchvision.transforms.Compose`.
"""
import copy
from functools import reduce
import importlib
import operator
import os

from absl import app, flags, logging
import numpy as np
from ruamel.yaml import YAML
from ruamel.yaml.comments import CommentedMap, CommentedSeq
import torch
from torchvision.transforms import Compose as TorchvisionCompose

from junction.augment.transforms import Compose
from junction.data_io import (CILRoadSegmentationDataset, dump_ids,
                              dump_predictions, dump_preprocessed_data,
                              load_raw_data, load_preprocessed_data)
from junction.infer import InferenceEngine
from junction.postprocess.hooks import (TrainPreInferenceHook,
                                        TrainPostInferenceHook)
from junction.train import TrainingEngine
from junction.utils import check_mkdir, shape, train_val_split

FLAGS = flags.FLAGS

# define the general flags for this app; each module can define its own
# additional flags
flags.DEFINE_string('config', None,
                    'The YAML config which sets up this driver.')
flags.mark_flag_as_required('config')
flags.DEFINE_boolean('no_seed', False,
                     'A manual seed should not be configured.')
flags.DEFINE_boolean('debugging', False, 'Debugging switch.')
flags.DEFINE_string('debug_save_dir', '/tmp/junction/debug',
                    'Directory where debugging data should be dumped.')
flags.DEFINE_integer(
        'debug_random_state',
        42,  # some fixed value
        'The random state used in debugging code. This is useful if, for '
        'instance, we want to ensure that the same points are sampled in the '
        'before and after debug hooks that wrap a transform. By default it '
        'is set.')


def main(argv, seed=42):
    r"""Main driver function."""
    del argv  # UNUSED
    # parse the config YAML file
    config = parse_config(FLAGS.config)
    config = check_config(config)
    # general settings
    save_dir = os.path.join(config['save_dir_root'], config['run_id'])
    save_dir = check_mkdir(save_dir, increment=True)

    # random seed
    if not FLAGS.no_seed:
        np.random.seed(seed)
        torch.manual_seed(seed)

    # save the config file in the save directory
    with open(os.path.join(save_dir, 'config.yaml'), 'w') as f:
        yaml = YAML()
        original_config = yaml.load(open(FLAGS.config, 'r'))
        yaml.dump(original_config, f)

    # go through each action to run
    for action in config['run_actions']:
        # load the data
        if config['data']['type'] == 'raw':
            data, ids = load_raw_data(config['data']['path'], which=action)
        else:
            data = load_preprocessed_data(config['data']['path'], which=action)
            ids = (np.arange(data[0].shape[0]), np.arange(data[1].shape[0]))
        logging.debug('Source data shape: %s', shape(data))

        if action == 'preprocessing':
            do_preprocessing(data, config, save_dir)
        elif action == 'training':
            do_training(data, ids, config, save_dir)
        else:
            assert action == 'inference'
            do_inference(data, ids, config, save_dir)


def do_preprocessing(data, config, save_dir):
    r"""Drives the preprocessing."""
    # run the corresponding chain of transformations
    transform = Compose(config['preprocessing']['transforms'])
    data = transform(data)
    logging.debug('Post-processing data shape: %s', shape(data))

    # save it
    dump_preprocessed_data(data, save_dir)


def do_training(data, ids, config, save_dir):
    r"""Drives the training session."""
    X_ids, y_ids = ids
    assert np.all(X_ids == y_ids)

    # run the corresponding chain of transformations
    transform = Compose(config['training']['transforms'])
    data = transform(data)
    logging.debug('Post-processing data shape: %s', shape(data))

    # split data into train/val
    (X_train, X_val), (y_train, y_val), (ids_train, ids_val) = \
            train_val_split(*data, X_ids,
                            val_size=config['training']['val_size'],
                            shuffle=True)
    # create the train/validate datasets
    train_dataset = CILRoadSegmentationDataset(X_train, y_train)
    val_dataset = CILRoadSegmentationDataset(X_val, y_val)

    # configure the training engine and run it
    training_engine = TrainingEngine(
            **config['training']['args'], save_dir=save_dir)
    training_engine(train_dataset, val_dataset)

    # save the IDs
    dump_ids(ids_train, path=os.path.join(save_dir, 'X_test_ids.txt'))
    dump_ids(ids_val, path=os.path.join(save_dir, 'X_val_ids.txt'))


def do_inference(data, ids, config, save_dir):
    r"""Drives the inference session.

    NOTE: For `ids` to make sense, it's crucial that the following code does
    not shuffle the data returned by the loading function.
    """
    # unpack data
    X, y, X_test = data
    X_ids, y_ids, X_test_ids = ids
    assert np.all(X_ids == y_ids)

    # pre-inference hooks
    pre_inference_hooks = \
            TorchvisionCompose(config['inference']['pre_run_hooks'])
    X_test, _ = pre_inference_hooks((X_test, None))
    logging.debug('Data shape after pre-inference hooks: %s',
                  shape((X_test, None)))

    # prepare the datasets as expected
    train_dataset = CILRoadSegmentationDataset((X, y))
    test_dataset = CILRoadSegmentationDataset(X_test)

    # construct the inference engine and run it
    inference_engine = InferenceEngine(**config['inference']['args'])
    predictions = inference_engine(test_dataset)
    logging.debug('Predictions shape: %s', shape(predictions))

    # dump raw predictions
    assert isinstance(predictions, np.ndarray)
    dump_predictions(
            predictions,
            save_dir,
            X_test_ids,
            file_name='y_pred_test_raw.npy',
            ids_file_name='X_test_ids.txt')

    # post-inference hooks
    post_inference_hooks = \
            TorchvisionCompose(config['inference']['post_run_hooks'])
    predictions = post_inference_hooks(predictions)
    logging.debug('Predictions shape after post-inference hooks: %s',
                  shape(predictions))

    # save the post-inference data
    dump_predictions(predictions, save_dir, X_test_ids)

    # save predictions on the (original) train dataset
    predictions = TorchvisionCompose([
            TrainPreInferenceHook(),
            InferenceEngine(**config['inference']['args']),
            TrainPostInferenceHook()
    ])(train_dataset)
    dump_predictions(
            predictions,
            save_dir,
            X_ids,
            file_name='y_pred_train_raw.npy',
            ids_file_name='X_train_ids.txt')


def parse_config(config_path):
    r"""Parses a YAML config file into a Python dict."""
    yaml = YAML()
    config = yaml.load(open(config_path, 'r'))
    classes = {}

    def get_from_dict(dataDict, mapList):  # pylint: disable=missing-docstring
        return reduce(operator.getitem, mapList, dataDict)

    def set_in_dict(dataDict, mapList, value):  # pylint: disable=missing-docstring
        get_from_dict(dataDict, mapList[:-1])[mapList[-1]] = value

    # assume if a function definition is in the most outer scope, it will not
    # have anywhere in config a nested class
    def walk(node, curr=None, depth=0):  # pylint: disable=missing-docstring
        if curr is None:
            curr = {}
        if isinstance(node, CommentedMap):
            for key, item in node.items():
                curr[depth] = key
                if key in ['object', 'closure']:
                    if depth in classes.keys():
                        classes[depth].append(copy.deepcopy(curr))
                    else:
                        classes[depth] = [copy.deepcopy(curr)]
                walk(item, copy.deepcopy(curr), depth + 1)
        elif isinstance(node, CommentedSeq):
            for i, _ in enumerate(node):
                curr[depth] = i
                walk(node[i], copy.deepcopy(curr), depth + 1)

    walk(config)
    depths = classes.keys()
    depths = reversed(sorted(depths))
    for depth in depths:
        for keys_dict in classes[depth]:
            keys = list(keys_dict.values())
            item = get_from_dict(config, keys)
            parts = item['name'].split('.')
            module = importlib.import_module('.'.join(parts[:-1]))

            # get the constructor for this instance
            ctor = getattr(module, parts[-1])
            # unify branches by setting an empty dict if no params provided
            if 'params' not in item or item['params'] is None:
                item['params'] = {}

            try:
                instance = ctor(**item['params'])
            except TypeError:
                # NOTE: There is an warning here, but it's a pylint bug, so we
                # keep these to supress it.
                # pylint: disable=cell-var-from-loop
                instance = lambda *args, f=ctor, params=item['params'], \
                        **kwargs: f(*args, **kwargs, **params)
                # pylint: enable=cell-var-from-loop
            set_in_dict(config, keys[:-1], instance)

    return config


def check_config(config):
    r"""Performs sanity checks on the config and does various manual
    adjustments.
    """
    # sanity checks
    if config['run_actions'] is None:
        raise ValueError('At least one action must be provided.')
    if 'preprocessing' in config['run_actions'] \
            and 'training' in config['run_actions']:
        raise ValueError(
                'It is disallowed to have both "preprocessing" and "training" '
                'to be run at once. This is simply because the same list of '
                'transforms corresponding to data preprocessing can be added '
                'to the pre-training list of transforms. We use '
                '"preprocessing" mainly for our convenience, to avoid '
                'repeating the same transforms in each training session.')

    # manual adjustments
    if 'preprocessing' in config['run_actions'] and \
            config['preprocessing']['transforms'] is None:
        config['preprocessing']['transforms'] = []
    if 'training' in config['run_actions'] and \
            config['training']['transforms'] is None:
        config['training']['transforms'] = []
    if 'inference' in config['run_actions']:
        if config['inference']['pre_run_hooks'] is None:
            config['inference']['pre_run_hooks'] = []
        if config['inference']['post_run_hooks'] is None:
            config['inference']['post_run_hooks'] = []

    return config


if __name__ == '__main__':
    # configure logging
    logging.set_verbosity(logging.DEBUG)

    # launch it
    app.run(main)
