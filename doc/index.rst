.. junction documentation master file, created by
   sphinx-quickstart on Wed Jun 27 14:54:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to junction's documentation!
====================================

***************************************************
Models - Fully Convolutional Networks (aka "UNets")
***************************************************
.. automodule:: junction.models.unet
    :members:
    :undoc-members:
    :show-inheritance:


*****************
Data Augmentation
*****************
.. automodule:: junction.augment.transforms
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: junction.augment.utils
    :members:
    :undoc-members:
    :show-inheritance:


***********
Postprocess
***********
.. automodule:: junction.postprocess.hooks
    :members:
    :undoc-members:
    :show-inheritance:


*********
Framework
*********
.. automodule:: junction.train
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: junction.infer
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: junction.loss
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: junction.data_io
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: junction.utils
    :members:
    :undoc-members:
    :show-inheritance:


***********
References
***********
.. bibliography:: refs.bib

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
