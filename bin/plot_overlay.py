import argparse
import sys

import cv2
import matplotlib.pyplot as plt
import numpy as np
from junction.data_io import load_raw_data


def main():
    # parse arguments
    args = parse_args()

    # load predictions
    preds = np.squeeze(np.load(args.pred_file))[args.indices]

    # load images
    (_, _, images), (_, _, ids) = \
            load_raw_data(args.raw_data_path, which='inference')
    argsort_ids = np.argsort(ids)
    images = images[argsort_ids][args.indices]

    n_rows = args.num_per_row
    n_cols = images.shape[0] // n_rows
    fig, axes = plt.subplots(
            nrows=n_rows,
            ncols=n_cols,
            figsize=(5 * n_cols, 5 * n_rows),
            sharex=True,
            sharey=True)
    k = 0
    for i in range(n_rows):
        for j in range(n_cols):
            # prepare color
            colors = np.zeros(preds[k].shape + (4, ))
            colors[preds[k] == 1] = plt.cm.Reds(1.0, alpha=0.6)

            axes[i, j].imshow(images[k], interpolation='none')
            axes[i, j].imshow(colors, interpolation='none')
            axes[i, j].set_axis_off()

            k += 1

    plt.tight_layout(pad=0, w_pad=0, h_pad=0)
    plt.savefig('overlay.pdf')


def parse_args():
    parser = argparse.ArgumentParser(
            description='Plot predictions overlayed with original images.')
    parser.add_argument(
            '--pred_file',
            type=str,
            required=True,
            help='The path to the numpy-saved predictions file.')
    parser.add_argument(
            '--raw_data_path',
            type=str,
            required=True,
            help='The path to the root of the raw data directory.')
    parser.add_argument(
            '--indices',
            nargs='+',
            type=int,
            required=True,
            help='Indices of images to plot.')
    parser.add_argument(
            '--num_per_row',
            type=int,
            required=True,
            help='Number of images to show per row.')

    return parser.parse_args()


if __name__ == '__main__':
    sys.exit(main())
