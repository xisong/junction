import argparse
import glob
import os
import sys


def main():
    parser = argparse.ArgumentParser(description='Renaming images.')
    parser.add_argument(
            '--path',
            type=str,
            required=True,
            help='The path of the images to rename.')
    args = parser.parse_args()

    for file_path in glob.glob(os.path.join(args.path, '*.png')):
        split = os.path.basename(file_path).split('_')[-1]
        os.rename(file_path, os.path.join(os.path.dirname(file_path), split))


if __name__ == '__main__':
    sys.exit(main())
