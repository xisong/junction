import argparse
import sys

import matplotlib.pyplot as plt
import numpy as np


def main():
    parser = argparse.ArgumentParser(
            description='Plot several predictions in a grid')
    parser.add_argument(
            '--predictions_file',
            type=str,
            required=True,
            help='The predictions file.')
    args = parser.parse_args()
    n_bins = 100

    y_pred = np.load(args.predictions_file)
    n = y_pred.shape[0]
    y_pred = y_pred.reshape(-1)
    values, base = np.histogram(y_pred, bins=n_bins)
    cdf = np.cumsum(values)

    plt.bar(base[:-1], values / n, width=1/n_bins, label='histogram')
    plt.plot(base[:-1], cdf / n, c='green', label='cdf')
    plt.legend()
    plt.tight_layout()
    plt.savefig('histogram.pdf')


if __name__ == '__main__':
    sys.exit(main())
