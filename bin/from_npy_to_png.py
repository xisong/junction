
import argparse
import os
import sys
import numpy as np

from junction.augment.utils import save_image
from junction.utils import check_mkdir


def main():
    parser = argparse.ArgumentParser(description='Read npy datasets and save images as png.')
    parser.add_argument(
            '--path',
            type=str,
            required=True,
            help='The path of the folder that contains X.npy and y.npy. E.g /tmp/junction/test_experiment_1/preprocessed_data/training')
    args = parser.parse_args()

    X = np.load(os.path.join(args.path, 'X.npy'))
    Y = np.load(os.path.join(args.path, 'y.npy'))

    png_dir = check_mkdir(
        os.path.join(args.path, 'png'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'images'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'groundtruth'), increment=False)

    index = 0
    for x in X:
        save_image(x, png_dir, str(index), 'png', "images")
        index += 1

    index = 0
    for y in Y:
        save_image(y, png_dir, str(index), 'png', "groundtruth")
        index += 1



if __name__ == '__main__':
    sys.exit(main())
