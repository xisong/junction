"""
Usage example:
python bin/dijkstra.py --path data/raw/ --predictions data/predictions/unet38-augumented-6000/100/ --idx 13 --dataset test --num_iters 1000 --min_prob 0.00001
"""
import argparse
import heapq
import os
import sys
import numpy as np
import torch

from junction.augment.filters import EliminateSpotsFilter
from junction.augment.utils import save_image
from junction.data_io import load_raw_data
from junction.postprocess.hooks import ToSubmissionFormat
from junction.utils import check_mkdir
from scipy import misc

import matplotlib.pyplot as plt

class ShortestPath(object):

    def __init__(self, mat):
        self.mat = mat
        self.log_mat = np.log(mat)
        self.n_rows = mat.shape[0]
        self.n_cols = mat.shape[1]

        self.road = []
        for r in range(self.n_rows):
            for c in range(self.n_cols):
                if self.mat[(r,c)] > 0.9:
                    self.road.append((r,c))

    def sample_road_pixel(self):
        return self.road[np.random.randint(len(self.road))]

    def is_valid(self, x):
        r, c = x
        return r >= 0 and r < self.n_rows and c >= 0 and c < self.n_cols

    def neighbors(self, x):
        r, c = x
        if self.is_valid((r-1, c)): yield (r-1, c), 1
        if self.is_valid((r+1, c)): yield (r+1, c), 1
        if self.is_valid((r, c-1)): yield (r, c-1), 1
        if self.is_valid((r, c+1)): yield (r, c+1), 1
        if self.is_valid((r-1, c-1)): yield (r-1, c-1), 1.414
        if self.is_valid((r-1, c+1)): yield (r-1, c+1), 1.414
        if self.is_valid((r+1, c-1)): yield (r+1, c-1), 1.414
        if self.is_valid((r+1, c+1)): yield (r+1, c+1), 1.414

    def dijkstra(self, start, target):
        heap = []

        visited = {}
        prev = {}

        heapq.heappush(heap, (0, start, None))

        while len(heap) > 0:
            dist, x, prev_x = heapq.heappop(heap)
            if x in visited:
                continue

            visited[x] = True
            prev[x] = prev_x

            if x == target:
                ret = []
                curr = target

                while curr != start:
                    ret.append(curr)
                    curr = prev[curr]
                ret.append(start)
                ret.reverse()

                return dist, ret

            for y, length in self.neighbors(x):
                new_dist = dist - self.log_mat[y]
                heapq.heappush(heap, (new_dist, y, x))

        print('Could not find path')
        return None

def main():
    parser = argparse.ArgumentParser(description='Read npy datasets and save images as png.')
    parser.add_argument(
            '--path',
            type=str,
            required=True,
            help='The path of the folder that contains X.npy and y.npy. E.g /tmp/junction/test_experiment_1/preprocessed_data/training')
    parser.add_argument(
            '--predictions',
            type=str,
            required=True,
            default=None,
            help='The path of the folder that contains predictions.')
    parser.add_argument(
            '--idx',
            type=int,
            required=True,
            help='Index of image to show')
    parser.add_argument(
            '--dataset',
            type=str,
            default='training',
            required=False,
            help='Index of image to show')
    parser.add_argument(
            '--num_iters',
            type=int,
            required=True,
            help='Number of iterations')
    parser.add_argument(
            '--min_prob',
            type=float,
required=True,
            help='Minimum probability path in dijkstra')
    args = parser.parse_args()
    png_dir = check_mkdir(
        os.path.join(args.path, 'pred_png'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'images'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'groundtruth'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'overlay'), increment=False)

    (X, Y, X_test), (X_ids, y_ids, X_test_ids) = load_raw_data(args.path, 'inference')

    if args.dataset == 'test':
        X = X_test
        X_ids = X_test_ids
        Y = np.zeros_like(Y, dtype=float)
        id_file = 'X_test_ids.txt'
        pred_file = 'y_pred_test_raw.npy'
    else:
        Y = Y.astype(float)
        id_file = 'X_train_ids.txt'
        pred_file = 'y_pred_train_raw.npy'

    Y_pred = np.load(os.path.join(args.predictions, pred_file))
    Y_pred = np.squeeze(Y_pred)
    tot_samples = X.shape[0]

    #Y_pred, _ = EliminateSpotsFilter()((Y_pred, None))

    pred_ids = []
    with open(os.path.join(args.predictions, id_file)) as f:
        for i, line in enumerate(f):
            pred_ids.append(int(line[:-1]))

    pos = {}
    for i in range(tot_samples):
        pos[X_ids[i]] = i

    tmp = Y_pred.copy()
    for i, x in enumerate(pred_ids):
        Y_pred[pos[x]] = tmp[i]

    Y /= 255.0

    if args.idx is not None:
        sub_noisy = ToSubmissionFormat(None)(Y_pred)[args.idx]
        sub_cut = ToSubmissionFormat(0.3)(Y_pred)[args.idx]

        tmp_noisy = np.zeros((38, 38)) if args.dataset == 'test' else np.zeros((25, 25))
        tmp_cut = np.zeros((38, 38)) if args.dataset == 'test' else np.zeros((25, 25))

        for j, i, label in sub_noisy:
            assert i % 16 == 0 and j % 16 == 0
            tmp_noisy[int(i/16), int(j/16)] = label
        for j, i, label in sub_cut:
            assert i % 16 == 0 and j % 16 == 0
            tmp_cut[int(i/16), int(j/16)] = label

        old_tmp_noisy = tmp_noisy.copy()

        f, axes = plt.subplots(1, 3, figsize=(15,15))

        for i, ax in enumerate(axes):
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            if i != 0:
                ax.grid(color='white', linestyle='-', linewidth=1, alpha=0.4)

        spath = ShortestPath(tmp_noisy)

        for it in range(100):
            for r in range(38):
                for c in range(38):
                    neigh_probs = [tmp_noisy[r,c]]
                    for (nr, nc), _ in spath.neighbors((r,c)):
                        neigh_probs.append(tmp_noisy[nr,nc])
                    neigh_probs = np.array(neigh_probs)

        for r in range(38):
            for c in range(38):
                tmp_cut[r,c] = 1 if tmp_noisy[r,c] > 0.2 else 0

        for it in range(args.num_iters):
            start = spath.sample_road_pixel()
            target = spath.sample_road_pixel()

            dist, pixels = spath.dijkstra(start, target)
            path_prob = np.exp(-dist)

            if it % 50 == 0:
                print(it, start, target, len(pixels), path_prob)

            if path_prob < args.min_prob:
                continue

            assert pixels is not None
            for r, c in pixels:
                #tmp_cut[(r, c)] = 0.5
                tmp_cut[(r, c)] = max(tmp_cut[(r,c)], 0.5)

        axes[0].imshow(X[args.idx])
        axes[1].imshow(old_tmp_noisy)
        axes[2].imshow(tmp_cut)
        plt.show(block=True)



if __name__ == '__main__':
    sys.exit(main())
