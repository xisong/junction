"""
Usage example:
python bin/visualize_errors.py --path data/raw/ --predictions data/predictions/unet38-augumented-gamma/ --idx 100
"""
import argparse
import os
import sys
import numpy as np
import torch

from junction.augment.utils import save_image
from junction.data_io import load_raw_data
from junction.postprocess.hooks import ToSubmissionFormat
from junction.utils import check_mkdir
from scipy import misc

import matplotlib.pyplot as plt

def main():
    parser = argparse.ArgumentParser(description='Read npy datasets and save images as png.')
    parser.add_argument(
            '--path',
            type=str,
            required=True,
            help='The path of the folder that contains X.npy and y.npy. E.g /tmp/junction/test_experiment_1/preprocessed_data/training')
    parser.add_argument(
            '--predictions',
            type=str,
            required=True,
            default=None,
            help='The path of the folder that contains predictions.')
    parser.add_argument(
            '--idx',
            type=int,
            required=True,
            help='Index of image to show')
    parser.add_argument(
            '--dataset',
            type=str,
            default='training',
            required=False,
            help='Index of image to show')
    args = parser.parse_args()
    png_dir = check_mkdir(
        os.path.join(args.path, 'pred_png'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'images'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'groundtruth'), increment=False)
    check_mkdir(
        os.path.join(png_dir, 'overlay'), increment=False)

    (X, Y, X_test), (X_ids, y_ids, X_test_ids) = load_raw_data(args.path, 'inference')

    if args.dataset == 'test':
        X = X_test
        X_ids = X_test_ids
        Y = np.zeros_like(Y, dtype=float)
        id_file = 'X_test_ids.txt'
        pred_file = 'y_pred_test_raw.npy'
    else:
        Y = Y.astype(float)
        id_file = 'X_train_ids.txt'
        pred_file = 'y_pred_train_raw.npy'

    Y_pred = np.load(os.path.join(args.predictions, pred_file))
    Y_pred = np.squeeze(Y_pred)
    tot_samples = X.shape[0]

    pred_ids = []
    with open(os.path.join(args.predictions, id_file)) as f:
        for i, line in enumerate(f):
            pred_ids.append(int(line[:-1]))

    pos = {}
    for i in range(tot_samples):
        pos[X_ids[i]] = i

    tmp = Y_pred.copy()
    for i, x in enumerate(pred_ids):
        Y_pred[pos[x]] = tmp[i]

    Y /= 255.0

    if args.idx is not None:
        sub = ToSubmissionFormat(0.3)(Y_pred)[args.idx]
        tmp = np.zeros((38, 38)) if args.dataset == 'test' else np.zeros((25, 25))

        for j, i, label in sub:
            assert i % 16 == 0 and j % 16 == 0
            tmp[int(i/16), int(j/16)] = label
        print(tmp)

        z = np.zeros((400, 400,3))

        if args.dataset == 'training':
            for i in range(400):
                for j in range(400):
                    t_pred = 0 if Y_pred[args.idx,i,j] < 0.3 else 1
                    t_true = 0 if Y[args.idx,i,j] < 0.3 else 1

                    if t_pred != t_true:
                        delta = Y_pred[args.idx,i,j] - Y[args.idx,i,j]

                        if delta > 0:
                            z[i,j,:] = np.array([255 * delta, 0, 0])
                        else:
                            z[i, j, :] = np.array([0, 0, -255 * delta])

        f, axes = plt.subplots(1, 5, figsize=(15,15))

        for i, ax in enumerate(axes):
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            if i != 0:
                ax.grid(color='white', linestyle='-', linewidth=1, alpha=0.4)

        axes[0].imshow(X[args.idx])
        axes[1].imshow(Y[args.idx])
        axes[2].imshow(Y_pred[args.idx])
        axes[3].imshow(z)
        axes[4].imshow(tmp)

        plt.show(block=True)
        exit(0)

    # Z = X.copy()
    #
    # for idx in range(Y.shape[0]):
    #     for i in range(400):
    #         for j in range(400):
    #             if Y[idx,i,j] > 0:
    #                 Z[idx,i,j,:] = np.array([255, 255, 255])
    #
    #             t_pred = 0 if Y_pred[idx,i,j] < 0.3 else 1
    #             t_true = 0 if Y[idx,i,j] < 0.3 else 1
    #
    #             if t_pred != t_true:
    #                 delta = abs(Y_pred[idx,i,j] - Y[idx,i,j])
    #                 Z[idx,i,j,:] = np.array([255 * delta, 0, 0])
    #
    #     save_image(Z[idx], png_dir, str(idx), 'png', "overlay")



if __name__ == '__main__':
    sys.exit(main())
