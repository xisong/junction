import argparse
import sys

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.rcParams.update({'font.size': 2})


def main():
    parser = argparse.ArgumentParser(
            description='Plot several predictions in a grid')
    parser.add_argument(
            '--predictions_file',
            type=str,
            required=True, help='The predictions file.')
    parser.add_argument(
            '--n_samples',
            type=int,
            default=81,
            help='The number of samples to plot.')
    args = parser.parse_args()

    # load gts and predictions
    y_pred = np.squeeze(np.load(args.predictions_file))[:args.n_samples]

    n_cols = int(np.sqrt(args.n_samples))
    n_rows = int(np.ceil(args.n_samples / n_cols))
    _, axes = plt.subplots(
            nrows=n_rows,
            ncols=n_cols,
            figsize=(n_cols, n_rows),
            sharex=True,
            sharey=True)

    k = 0
    for i in range(n_rows):
        for j in range(n_cols):
            axes[i, j].imshow(y_pred[k])
            axes[i, j].set_axis_off()
            k += 1

    plt.tight_layout(pad=0, w_pad=0, h_pad=0)
    plt.savefig('predictions.pdf')


if __name__ == '__main__':
    sys.exit(main())
