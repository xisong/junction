import argparse
import os
import sys

import numpy as np
from junction.data_io import dump_predictions
from junction.postprocess.hooks import ToClassLabels, ToSubmissionFormat

# name of the raw predictions file
INPUT_PREDICTIONS_FILE = 'y_pred_test_raw.npy'
# name of the ids file
INPUT_IDS_FILE = 'X_test_ids.txt'

# global transformers
FORMAT_TO_TRANSFORM_FN = {
        'npy': lambda threshold: ToClassLabels(threshold),
        'csv': lambda threshold: ToSubmissionFormat(threshold),
}

# file names
FORMAT_TO_FILE_NAME = {
        'npy': 'y_pred_ensemble.npy',
        'csv': 'submission_ensemble.csv',
}


def main():
    # load predictions
    args = parse_args()
    preds = [np.load(os.path.join(p, INPUT_PREDICTIONS_FILE))
             for p in args.paths]
    ids = [np.loadtxt(os.path.join(p, INPUT_IDS_FILE), dtype=np.uint8)
           for p in args.paths]

    # get them in the same order
    preds = [pred[np.argsort(indices)] for pred, indices in zip(preds, ids)]

    # get average prediction
    avg_pred = np.stack(preds).mean(axis=0)

    # save it
    dump_predictions(
            FORMAT_TO_TRANSFORM_FN[args.output_format](args.threshold)(avg_pred),
            ids=np.sort(ids[0]),
            path='.',
            file_name=FORMAT_TO_FILE_NAME[args.output_format])


def parse_args():
    parser = argparse.ArgumentParser(description='Average predictions.')
    parser.add_argument(
            '--paths',
            nargs='+',
            type=str,
            required=True,
            help='Paths to model output directories.')
    parser.add_argument(
            '--threshold',
            type=float,
            default=0.3,
            help='The threshold used to get binary labels in case raw '
            'predictions are given')
    parser.add_argument(
            '--output_format',
            choices=['npy', 'csv'],
            default='npy',
            help='The submission format to output.')

    return parser.parse_args()


if __name__ == '__main__':
    sys.exit(main())
